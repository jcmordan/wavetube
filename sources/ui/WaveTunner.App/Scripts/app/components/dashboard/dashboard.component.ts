import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';

import {NextEventsComponent} from '../event/widgets/next_events.component';

@Component({
    selector: 'my-dashboard',
    templateUrl: 'Scripts/app/components/dashboard/dashboard.component.html',
    directives: [NextEventsComponent]
})
export class DashboardComponent implements OnInit {
    constructor(private _router:Router) {
    }

    ngOnInit() {


    }

}

import { Component, OnInit } from '@angular/core';
import {Router, RouteParams, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import { Observable } from 'rxjs/Observable';


import { AthleteService } from '../../../services/athlete.service';
import { GeneralService } from '../../../services/general.service';
import { Athlete } from '../../../core/athlete';
import { IdentificationType } from '../../../core/identification-type'


import 'moment';
import 'daterangepicker';
import {Category} from "../../../core/category";
import {AthleteCategory} from "../../../core/athlete_category";
import {SystemInfo} from "../../../system_info";

// jQuery is loaded independently on its own
// in BundleConfig.cs (without SystemJS).
// No need to import it.
declare var $ : any;

@Component({
    selector: 'athlete-editor',
    templateUrl: 'Scripts/app/components/athlete/widgets/athlete-edit.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class AthleteEditComponent implements OnInit {
    public title: string = 'Create Athlete';
    public athlete: Athlete = new Athlete();
    public identificationTypes: IdentificationType[];
    public availablesCategories: Category[] = [];
    public selectedAvailableCategory: Category;
    public selectedAssignedCategory: Category;

    constructor(
        private _router: Router,
        private  _routeParams : RouteParams,
        private _athleteService: AthleteService,
        private _generalService: GeneralService,
        private _appConfig: SystemInfo
    ){}

    ngOnInit() {
        let param = (this._routeParams.get('id'));
        let athleteId = param ? +param : 0;

        Observable.forkJoin(
            this.getAthlete(athleteId),
            this.getIdentificationTypes(),
            this.getCategories()
        ).subscribe(data =>{
            this.athlete = data[0];
            this.identificationTypes = data[1];

            if(!this.athlete.categories || this.athlete.categories.length === 0){
                this.availablesCategories = data[2];
            }else {
                var athleteCategories = this.athlete.categories.map(ac => {
                    return ac.category;
                });
                this.availablesCategories = data[2].filter(cat =>{
                    return !athleteCategories.some(ac => {
                        return ac.categoryId === cat.categoryId;
                    });
                });
            }

            this.initilizeDatePiker();
        });
    }

    private getCategories(){
        return this._generalService.getCategories();
    }

    private getIdentificationTypes() {
        return this._generalService.getIdentifiacitonTypes();
    }

    private getAthlete(athleteId: number) {
        if (athleteId === 0) {
            this.title = 'Create new Athlete';

            this.athlete.categories = [];
            return Observable.of(this.athlete);
        }
        this.title = 'Edit Athlete';
        return this._athleteService.getAthlete(athleteId);
    }
   
    onSelectAvailableCategory(category: Category){
        this.selectedAvailableCategory = category;
    }

    onSelectAssignedCategory(category: Category){
        this.selectedAssignedCategory = category;
    }

    assignSelectedCategory(){
        if(!this.selectedAvailableCategory){
            return;
        }

        var category = new AthleteCategory(this.athlete.athleteId, this.selectedAvailableCategory.categoryId);
        category.category = this.selectedAvailableCategory;
        category.initialScore = 0.00;
        this.athlete.categories.push(category);
        this.availablesCategories.splice(this.availablesCategories.indexOf(this.selectedAvailableCategory), 1);
        this.selectedAssignedCategory = null;
        this.selectedAvailableCategory = null;
    }
    assignAllCategories(){
        this.availablesCategories.map(cat => {
            var category = new AthleteCategory(this.athlete.athleteId, cat.categoryId);
            category.category = cat;
            category.initialScore = 0.00;
            this.athlete.categories.push(category);
        });

        this.availablesCategories = [];
        this.selectedAssignedCategory = null;
        this.selectedAvailableCategory = null;
    }

    unAssignSelectedCategory(){
        if(!this.selectedAssignedCategory){
            return;
        }
        this.availablesCategories.push(this.selectedAssignedCategory);

        this.athlete.categories = this.athlete.categories.filter(cat =>{
            if(cat.categoryId === this.selectedAssignedCategory.categoryId){
                return false;
            }
            return true;
        });
        this.selectedAssignedCategory = null;
        this.selectedAvailableCategory = null;
    }
    unAssignAllCategories(){
        this.athlete.categories.map(cat => {
            this.availablesCategories.push(cat.category);
        });

        this.athlete.categories = [];
        this.selectedAssignedCategory = null;
        this.selectedAvailableCategory = null;
    }

    goBack() {
        this._router.navigate(['Athletes']);
    }

    onSave(){
        if(!this.athlete.athleteId ||
            this.athlete.athleteId === 0){

            this._athleteService.addAthlete(this.athlete)
                .subscribe(resp => {
                    this._appConfig.success('Athlete created successfully');
                    this._router.navigate(['Athletes']);
                });
            return;
        }

        this._athleteService.updateAthlete(this.athlete)
            .subscribe(resp => {
                this._appConfig.success('Athlete updated successfully');
            });
    }

    validateMail(){
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (EMAIL_REGEXP.test(this.athlete.email)) {
            return true;
        }
        return false;
    }

    initilizeDatePiker(){
        let athlete = this.athlete;
        if ($ && $().daterangepicker) {
            $('.date-range-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                locale: {
                    format: 'DD/MMM/YYYY'
                }
            }, function (start /*, end, label*/) {
                athlete.dateOfBirth = start.format('DD/MM/YYYY');
                // console.log(start.toISOString(), end.toISOString(), label);
            });
        }
        else{
            console.error('jQuery.datepicker not defined.');
        }
    }


}
import { Component, OnInit } from '@angular/core';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';

import { JqueryDatatableDirective }            from '../../directives/jquery-datatable.directive';
import { JqueryDataTableEvent } from '../../directives/jquery-datable-event';

import { AthleteService } from '../../services/athlete.service'
import { Athlete } from '../../core/athlete'


@Component({
    selector: 'athletes-content',
    templateUrl: 'Scripts/app/components/athlete/athlete.component.html',
    directives: [ROUTER_DIRECTIVES, JqueryDatatableDirective]
})

export class AthleteComponent implements OnInit {
    public athletes : Athlete[];
    public isLoading: boolean = true;
    public dataTableOptions      : any;
    public dataTableEvents: JqueryDataTableEvent[]= [];

    constructor(
        private _router: Router,
        private _athleteService: AthleteService) { }

    ngOnInit() {
        this.getAthletes();
    }

    getAthletes(){
        this.isLoading = true;
        this._athleteService.getAthletes()
            .subscribe(result => {
                this.athletes = result;
                this.initializeDatatable(this.athletes);
                this.isLoading = false;
            });
    }

    addNew(){
        this._router.navigate(['AthleteCreate']);
    }

    initializeDatatable(athletes: Athlete[])
    {
        if(athletes.length === 0){
            return;
        }

        var items = [];

        for (let c in athletes) {
            items.push({
                firtsName: athletes[c].firstName,
                lastName: athletes[c].lastName,
                identificationType: athletes[c].identificationType.description,
                identificationNumber: athletes[c].identificationNumber,
                active: athletes[c].active,
                id: athletes[c].athleteId,
            });
        }
        
        this.dataTableOptions = {
            "data"         : items,
           // "autoWidth"    : false,
            "columns"      : Object.keys(items[0]).map( columnName => { return { 'data' : columnName }; } ),
            "columnDefs"   : [
                {
                    "targets": -1,

                    render: function( data, type, full, meta ) {
                        return `<td>
		            				<div class="row">		            					
										<div class="col-lg-4">
											<a data-id="${full.id}" class="fa fa-pencil athlete-edit-link" data-toggle="tooltip" data-original-title="Action">
											</a>
										</div>
				            		</div>
		            			</td>`;
                    },
                },
            ]
        };

        this.defineDataTableEvent();
    }

    defineDataTableEvent() {
        var router = this._router;

        this.dataTableEvents.push({
            eventName: 'click',
            selector: '.athlete-edit-link',
            callback: function () {
                router.navigate(['AthleteEdit', {id: +this.attributes['data-id'].value}]);
            }
        });
    }
}

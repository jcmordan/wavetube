import { Component, OnInit } from '@angular/core';
import {Router, RouteParams, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import { Observable } from 'rxjs/Observable';
import {SystemInfo} from "../../../system_info";

// jQuery is loaded independently on its own
// in BundleConfig.cs (without SystemJS).
// No need to import it.
declare var $ : any;

import 'moment';
import 'daterangepicker';
import {User} from "../../../core/user";
import {GeneralService} from "../../../services/general.service";
import {UserService} from "../../../services/user.service";
import {IdentificationType} from "../../../core/identification-type";
import {Rol} from "../../../core/rol";

@Component({
    selector: 'athlete-editor',
    templateUrl: 'Scripts/app/components/user/widgets/user_edit.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class UserEditComponent implements OnInit {
    public title: string = 'Create User';
    public user: User = new User();
    public identificationTypes: IdentificationType[];
    public  roles: Rol[];

    constructor(
        private _router: Router,
        private  _routeParams : RouteParams,
        private _generalService: GeneralService,
        private _appConfig: SystemInfo,
        private _userService: UserService
    ){}

    ngOnInit() {
        let param = (this._routeParams.get('id'));
        let userId = param ? +param : 0;
        Observable.forkJoin(
            this.getUser(userId),
            this.getIdentificationTypes(),
            this._appConfig.getUserRoles()
        ).subscribe(data =>{
            this.user = data[0];
            this.identificationTypes = data[1];
            this.roles= data[2];
            this.initilizeDatePiker();
        });
    }

    getUser(userId:number){
        if(userId === 0){
            this.user.userId = 0;
            return Observable.of(this.user);
        }
        return this._userService.getUser(userId);
    }

    private getIdentificationTypes() {
        return this._generalService.getIdentifiacitonTypes();
    }

    goBack() {
        this._router.navigate(['Users']);
    }

    onSave(){
        if(this.user.userId === 0){
            this._userService.add(this.user)
                .subscribe(data =>{
                    this._appConfig.success('User created successfully');
                    this._router.navigate(['Users']);
                });
            return;
        }

        this._userService.edit(this.user)
            .subscribe(data =>{
                this._appConfig.success('User edited successfully');
            });
    }

    validateMail(){
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (EMAIL_REGEXP.test(this.user.email)) {
            return true;
        }
        return false;
    }

    initilizeDatePiker(){
        let user= this.user;
        if ($ && $().daterangepicker) {
            $('.date-range-picker').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                locale: {
                    format: 'DD/MMM/YYYY'
                }
            }, function (start /*, end, label*/) {
                user.dateOfBirth = start.format('DD/MM/YYYY');
                // console.log(start.toISOString(), end.toISOString(), label);
            });
        }
        else{
            console.error('jQuery.datepicker not defined.');
        }
    }


}
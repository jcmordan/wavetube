import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';

import {JqueryDatatableDirective}            from '../../directives/jquery-datatable.directive';
import {JqueryDataTableEvent} from '../../directives/jquery-datable-event';

import {UserService} from '../../services/user.service'
import {User} from '../../core/user'


@Component({
    selector: 'my-heroes',
    templateUrl: 'Scripts/app/components/user/user.component.html',
    directives: [ROUTER_DIRECTIVES, JqueryDatatableDirective]
})

export class UserComponent implements OnInit {
    public users:User[];
    public isLoading:boolean;
    public dataTableOptions:any;
    public dataTableEvents:JqueryDataTableEvent[] = [];

    constructor(private _router:Router,
                private _userService:UserService) {
    }

    ngOnInit() {
        this.getUsers();
    }

    getUsers() {
        this._userService.getUsers()
            .subscribe(result => {
                this.users = result;
                this.initializeDatatable(this.users);
            });
    }

    addNew(){
        this._router.navigate(['UserCreate']);
    }

    initializeDatatable(users:User[]) {
        if (users.length === 0) {
            return;
        }

        var items = [];

        for (let c in users) {
            items.push({
                username: users[c].userName,
                firtsName: users[c].firstName,
                lastName: users[c].lastName,
                identificationType: users[c].identificationType.description,
                identificationNumber: users[c].identificationNumber,
                rol: users[c].rolDescription,
                active: users[c].active,
                id: users[c].userId,
            });
        }

        this.dataTableOptions = {
            "data": items,
            "autoWidth": false,
            "columns": Object.keys(items[0]).map(columnName => {
                return {'data': columnName};
            }),
            "columnDefs": [
                {
                    "targets": -1,
                    render: function (data, type, full, meta) {
                        return `<td>
		            				<div class="row">		            					
										<div class="col-lg-4">
											<a data-id="${full.id}" class="fa fa-pencil edit-user-link" data-toggle="tooltip" data-original-title="Action">
											</a>
										</div>
				            		</div>
		            			</td>`;
                    },
                },
            ]
        };

        this.defineDataTableEvent()

        this.isLoading = false;
    }

    defineDataTableEvent() {
        var router = this._router;
        this.dataTableEvents.push({
            eventName: 'click',
            selector: '.edit-user-link',
            callback: function () {
                router.navigate(['UserEdit', {id: +this.attributes['data-id'].value}]);
            }
        });
    }
}

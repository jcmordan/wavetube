import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {Heat} from "../../../core/heat";
import {HeatService} from "../../../services/heat.service";
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {SystemInfo} from "../../../system_info";


declare var $:any;
@Component({
    selector: 'heat-positions',
    templateUrl: 'Scripts/app/components/heat/widgets/heat_positions.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class HeatPositions implements OnInit {
    @Input()
    public heat:Heat;

    constructor(private _router:Router,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        console.log(this.heat);
    }

    getAthleteColor(colorCode:number) {
        return this._systemInfo.getAthleteExaColor(colorCode);
    }

    getAthleteFirstBestWave(athleteId:number){
        return 5.5;
    }

    getAthleteSecondBestWave(athleteId:number){
        return 4.25;
    }

    getAthleteWaveAvg(athleteId:number){
        return (4.25 + 5.5)/ 2;
    }
}
import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {Heat} from "../../../core/heat";
import {HeatService} from "../../../services/heat.service";
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {SystemInfo} from "../../../system_info";

import {Observable} from 'rxjs/Observable';
import {HeatPanel} from "./heat_panel.component";


declare var $:any;
@Component({
    selector: 'previous-nex-heat',
    templateUrl: 'Scripts/app/components/heat/widgets/previous_next_heat.component.html',
    directives: [ROUTER_DIRECTIVES, HeatPanel]
})
export class PreviousNextHeat implements OnInit {
    @Input()
    public currentHeat:Heat;

    @Input()
    public widgetType:any;

    public heat:Heat;
    public title:string;

    constructor(private _router:Router,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        Observable.forkJoin(
            this.getHeat(this.currentHeat, this.widgetType.type)
        ).subscribe(
            (data) => {

                this.heat = data[0];
                if(!!this.heat){
                    return;
                }
                if (!this.heat.score) {
                    this.heat.score = [];
                }
            });

        this.title = this.widgetType.type === 'previous' ? 'Previous Heat' : 'Next Heat';
    }

    getHeat(currentHeat:Heat, type:string)
    {
        if(type === 'previous')
        {
            return this._heatService.getPreviousHeat(currentHeat.surfEventId);
        }

        return this._heatService.getNextHeat(currentHeat.surfEventId, currentHeat.round, currentHeat.heatId);
    }

    getAthleteColor(colorCode:number) {
        return this._systemInfo.getAthleteExaColor(colorCode);
    }
}
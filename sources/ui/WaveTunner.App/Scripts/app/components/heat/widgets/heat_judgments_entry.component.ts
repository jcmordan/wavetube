import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {Heat} from "../../../core/heat";
import {HeatScore} from "../../../core/heat_score"
import {HeatService} from "../../../services/heat.service";
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {Observable} from 'rxjs/Observable';
import {AgGridNg2} from 'ag-grid-ng2/main';
import {SystemInfo} from "../../../system_info";
import {HeatAthletePenalty} from "../../../core/heat_athlete_penalty";

declare var $:any;
@Component({
    selector: 'heat-entry',
    templateUrl: 'Scripts/app/components/heat/widgets/heat_judgments_entry.component.html',
    directives: [ROUTER_DIRECTIVES, AgGridNg2]
})
export class HeatEntry implements OnInit {
    public heat:Heat;
    public columnDefs:any[];
    public rowData:number[];
    showToolPanel:boolean = true;
    public tableModel:any [];


    constructor(private _router:Router,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        Observable.forkJoin(
            this.getCurrentHeat()
        ).subscribe(
            (data) => {
                this.heat = data[0];
                console.log(this.heat);
                if (!this.heat.scores) {
                    this.heat.scores = [];
                }

                this.defineEntryGrid(this.heat);
            });
    }

    getCurrentHeat() {
        return this._heatService.getCurrentHeat(3);
    }

    saveHeatScore(heatScore:HeatScore) {
        return this._heatService.saveHeatScore(heatScore);
    }

    saveHeatAthletePenalty(heatAthletePenalty:HeatAthletePenalty) {
        return this._heatService.saveHeatAthletePenalty(heatAthletePenalty);
    }


    getAthleteColor(colorCode:number) {
        return this._systemInfo.getAthleteExaColor(colorCode);
    }

    onCheackClick(athId:number, input:any) {


        if (Number(input.value) <= 0 || Number(input.value) > 10) {
            return;
        }
        for (var ath  of this.tableModel) {
            if (ath.athleteId == athId) {
                let keepGoing = true;
                if (input.value == "" || input.value == "0") {
                    return;
                }

                ath.score.forEach(scr => {
                    if (!keepGoing) {
                        return
                    }
                    if (scr.value === '' || !scr.permanent) {
                        scr.value = input.value;
                        keepGoing = false;

                        setTimeout(()=> {
                                scr.permanent = true;
                                Observable.forkJoin(
                                    this.saveHeatScore({
                                        heatScoreId: 0,
                                        heatId: this.heat.heatId,
                                        judgeId: 9,
                                        athleteId: athId,
                                        score: input.value,

                                    })
                                ).subscribe(
                                    (data) => {
                                    });
                                input.value = "";
                            }
                            , 5000);


                    }


                });
            }

        }

    }

    onPenaltyClick(athleteId:number) {
        Observable.forkJoin(
            this.saveHeatAthletePenalty({
                heatAthletePenaltyId: 0,
                heatId: this.heat.heatId,
                judgeId: 9,
                athleteId: athleteId,
                type: 1
            })
        ).subscribe(
            (data) => {
            });

        for (var ath  of this.tableModel) {
            if (ath.athleteId == athleteId) {
                ath.penalties.push({
                    type: 1
                });
            }
        }
    }

    defineEntryGrid(heat:Heat) {
        console.log(heat.athletes);
        this.rowData = [];
        for (let round = 1; round <= 15; round++) {
            this.rowData.push(round);
        }
        this.tableModel = heat.athletes.map(ath => {
            let table = {
                athleteId: ath.athleteId,
                colorCode: ath.colorCode,
                score: [],
                penalties: []
            };

            let round = 1;
            for (let count = 0; count < this.heat.scores.length; count++) {
                if (table.athleteId == this.heat.scores[count].athleteId) {
                    table.score.push({round: round, value: this.heat.scores[count].score, permanent: true});
                    round++;
                }

            }
            for (let count = 1 + table.score.length; count <= 15; count++) {
                table.score.push({round: count, value: '', permanent: false});
            }
            for (let count = 0; count < this.heat.penalties.length; count++) {
                if (table.athleteId == this.heat.penalties[count].athleteId) {
                    table.penalties.push({type : 1 });
                }

            }
            return table;
        });
        console.log(this.tableModel);
    }

}

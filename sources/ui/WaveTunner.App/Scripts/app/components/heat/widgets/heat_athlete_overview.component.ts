import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {Heat} from "../../../core/heat";
import {HeatService} from "../../../services/heat.service";
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {Observable} from 'rxjs/Observable';
import {AgGridNg2} from 'ag-grid-ng2/main';
import {SystemInfo} from "../../../system_info";
import {Athlete} from "../../../core/athlete";
import {HeatScore} from "../../../core/heat_score";
import {HeatAthlete} from "../../../core/heat_athlete";

declare var $:any;
@Component({
    selector: 'heat-judge-overview',
    templateUrl: 'Scripts/app/components/heat/widgets/heat_athlete_overview.component.html',
    directives: [ROUTER_DIRECTIVES, AgGridNg2]
})
export class HeatAthleteOverview implements OnInit {
    @Input()
    public heatAthlete:HeatAthlete;

    //@Input()
    public scores:HeatScore[] = [];

    public columnDefs:any[];
    public rowData:any[];
    public gridOptions:any;

    constructor(private _router:Router,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        this.defineEntryGrid(this.scores);
    }

    getAthleteColor(colorCode:number) {
        return this._systemInfo.getAthleteExaColor(colorCode);
    }

    getAthelteTextColor(colorCode:number) {
        let color:any = this._systemInfo.getAthleteExaColor(colorCode);
        return   0xffffff ^ color;
    }

    defineEntryGrid(scores:HeatScore[]) {
        let self = this, pad = "00";

        let index = 0;
        this.columnDefs = [{
            headerName: '#',
            field: 'wave',
            width: 25
        }];

        for (let i = 1; i < 5; i += 1) {
            let colName = 'J' + i.toString();
            this.columnDefs.push({
                headerName: colName,
                field: colName,
                width: 55
            });
        }
        this.columnDefs.push({
            headerName: 'Pr',
            field: 'Pr',
            width: 55
        });

        this.rowData = [];
        for (let i = 0; i < 15; i = i + 1) {
            this.rowData.push({
                wave:i+1,
                J1: '',
                J2: '',
                J3: '',
                J4: '',
                Pr: ''
            });
        }


        this.gridOptions = {
            columnDefs: this.columnDefs,
            rowData: this.rowData,

            enableColResize: false,
            enableSorting: false,
            enableFilter: false,
            singleClickEdit: true
        };
    }
}
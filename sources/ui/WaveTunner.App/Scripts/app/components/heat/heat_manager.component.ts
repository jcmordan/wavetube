import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {Observable} from 'rxjs/Observable';

import {HeatService} from "../../services/heat.service";
import {SystemInfo} from "../../system_info";

import {HeatAthleteOverview} from "./widgets/heat_athlete_overview.component";
import {Heat} from "../../core/heat";
import {HeatPositions} from "./widgets/heat_positions.component";
import {PreviousNextHeat} from "./widgets/previous_next_heat.component";


@Component({
    selector: 'heat-manager',
    templateUrl: 'Scripts/app/components/heat/heat_manager.component.html',
    directives: [HeatAthleteOverview, HeatPositions, PreviousNextHeat]
})

export class HeatManager implements OnInit {
    public heat:Heat;

    constructor(private _router:Router,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        Observable.forkJoin(
            this.getCurrentHeat()
        ).subscribe(
            (data) => {
                this.heat = data[0];
                if (!this.heat.score) {
                    this.heat.score = [];
                }
            });
    }

    getCurrentHeat() {
        return this._heatService.getCurrentHeat(2);
    }

    getAthleteColor(colorCode:number) {
        return this._systemInfo.getAthleteExaColor(colorCode);
    }
}
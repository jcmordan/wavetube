import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {Observable} from 'rxjs/Observable';

import {HeatService} from "../../services/heat.service";
import {SystemInfo} from "../../system_info";

import {HeatAthleteOverview} from "./widgets/heat_athlete_overview.component";
import {Heat} from "../../core/heat";



@Component({
    selector: 'heat-overview',
    templateUrl: 'Scripts/app/components/heat/heat_overview.component.html',
    directives: [HeatAthleteOverview]
})
export class HeatOverview implements OnInit {
    public heat:Heat;

    constructor(private _router:Router,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        Observable.forkJoin(
            this.getCurrentHeat()
        ).subscribe(
            (data) => {
                this.heat = data[0];
                if (!this.heat.score) {
                    this.heat.score = [];
                }
            });
    }

    getCurrentHeat() {
        return this._heatService.getCurrentHeat(9);
    }
}
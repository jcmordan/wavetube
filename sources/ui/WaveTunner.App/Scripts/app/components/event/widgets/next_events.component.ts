import { Component, OnInit } from '@angular/core';
import { Router, RouteParams } from '@angular/router-deprecated';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';
import { Observable } from 'rxjs/Observable';


import { SurftEventService } from '../../../services/surf_event.service';
import { SurfEvent } from '../../../core/surf_event'
import {SystemInfo} from "../../../system_info";


@Component({
    selector: 'next-events-content',
    templateUrl: 'Scripts/app/components/event/widgets/next_events.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class NextEventsComponent implements OnInit {
    public events: SurfEvent[] = [];
    constructor(
        private _router: Router,
        private  _routeParams : RouteParams,
        private _eventService: SurftEventService,
        private _systemInfo: SystemInfo
    ){}

    ngOnInit() {
        Observable.forkJoin(
            this.getNexEvents()
        ).subscribe(
            (data) => {
                this.events = data[0];
            }
        );
    }

    getNexEvents(){
      return  this._eventService.getNexEvents(this._systemInfo.nexEventsCount)
    }

    getMonthName(date: string){
        let d = new Date(date);
        return this._systemInfo.monthNames[d.getMonth()];
    }

    getDayOfMonth(date: string){
        let d = new Date(date);
        return d.getDate();
    }

    editEvent(eventId: number){
       this._router.navigate(['EventEdit', {id: eventId}]);
    }
}
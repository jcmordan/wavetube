import {Component, OnInit} from '@angular/core';
import {Router, RouteParams} from '@angular/router-deprecated';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {Observable} from 'rxjs/Observable';


import {SurftEventService} from '../../../services/surf_event.service';
import {GeneralService} from '../../../services/general.service';
import {SurfEvent} from '../../../core/surf_event';
import {EventStatus} from '../../../core/event_status';


import 'moment';
import 'daterangepicker';
import 'wizard';
import {SystemInfo} from "../../../system_info";
import {Category} from "../../../core/category";
import {EventCategory} from "../../../core/event_category";
import {Athlete} from "../../../core/athlete";
import {AthleteService} from "../../../services/athlete.service";
import {EventCategoryAthlete} from "../../../core/event_category_athlete";
import {HeatService} from "../../../services/heat.service";
import {Heat} from "../../../core/heat";
import {HeatPanel} from "../../heat/widgets/heat_panel.component";

// jQuery is loaded independently on its own
// in BundleConfig.cs (without SystemJS).
// No need to import it.
declare var $:any;

@Component({
    selector: 'athlete-editor',
    templateUrl: 'Scripts/app/components/event/widgets/event_edit.component.html',
    directives: [ROUTER_DIRECTIVES, HeatPanel]
})

export class EventEditorComponent implements OnInit {

    private _allCategories:Category[] = [];
    private _allAthletes:Athlete[] = [];

    public title:string = 'New Event';
    public selectedAvailableAthlete:Athlete;

    public surfEvent:SurfEvent = new SurfEvent();
    public eventStatus:EventStatus[] = [];
    public availablesCategories:Category[] = [];
    public selectedAvailableCategory:Category;
    public selectedAssignedCategory:Category;
    public availableAthletes:Athlete[] = [];
    public selectAssignedAthlete:Athlete;
    public selectedEventCategory:EventCategory;
    public selectedHeats:Heat[] = [];
    public selectedHeat:Heat;

    constructor(private _router:Router,
                private  _routeParams:RouteParams,
                private _generalService:GeneralService,
                private _eventService:SurftEventService,
                private _athleteService:AthleteService,
                private _heatService:HeatService,
                private _systemInfo:SystemInfo) {
    }

    ngOnInit() {
        let param = this._routeParams.get('id');
        let eventId = param ? +param : 0;

        this.title = eventId > 0 ? 'Edit Event' : 'New Event';
        Observable.forkJoin(
            this.getEvent(eventId),
            this.getEventStatus(),
            this.getCategories(),
            this.getAthletes()
        ).subscribe(
            (data) => {
                this.surfEvent = data[0];
                this.eventStatus = data[1];


                this._allCategories = data[2];
                this.setAvaileableCategories(this._allCategories);

                this._allAthletes = this._systemInfo.dynamicSort(data[3], 'fullName');
                this.setAvaileableAthletes(this._allAthletes)

                this.initilizeDatePiker();
                this.initializeWizard();
            });
    }

    getEvent(eventId:number) {
        if (eventId === 0) {
            return Observable.of(new SurfEvent());
        }

        return this._eventService.getEvent(eventId);
    }

    private getCategories() {
        return this._generalService.getCategories();
    }

    private getAthletes() {
        return this._athleteService.getAthletes();
    }

    private setAvaileableCategories(categories:Category[]) {
        if (!this.surfEvent.categories || this.surfEvent.categories.length === 0) {
            this.availablesCategories = categories;
        } else {
            var athleteCategories = this.surfEvent.categories.map(ac => {
                return ac.category;
            });
            this.availablesCategories = categories.filter(cat => {
                return !athleteCategories.some(ac => {
                    return ac.categoryId === cat.categoryId;
                });
            });
        }

        this.selectedAssignedCategory = null;
        this.selectedAvailableCategory = null;
    }

    private setAvaileableAthletes(athletes:Athlete[]) {
        if (!this.selectedEventCategory) {
            this.availableAthletes = [];
            return;
        }
        let self = this;


        self.availableAthletes = athletes.filter(athlete => {
            return athlete.categories.some(cat => {
                return cat.categoryId === self.selectedEventCategory.category.categoryId;
            })
        });

        if (!self.selectedEventCategory || self.selectedEventCategory.athletes.length == 0) {
            return;
        }

        for (var i = self.availableAthletes.length - 1; i >= 0; i = i - 1) {
            let athlete = self.availableAthletes[i];
            self.selectedEventCategory.athletes.forEach(ac => {
                if (athlete.athleteId === ac.athleteId) {
                    self.availableAthletes.splice(self.availableAthletes.indexOf(athlete), 1);
                }
            });
        }

        self.availableAthletes = this._systemInfo.dynamicSort(self.availableAthletes, 'fullName');
        self.selectedAvailableAthlete = null;
    }

    onSelectAvailableCategory(category:Category) {
        this.selectedAvailableCategory = category;
    }

    onSelectAssignedCategory(category:Category) {
        this.selectedAssignedCategory = category;
    }

    onSelecteEventCategory(category:EventCategory) {
        this.selectedEventCategory = category;
        this.setAvaileableAthletes(this._allAthletes);

        if (!!this.surfEvent.heats && this.surfEvent.heats.length > 0) {
            this.setSelectedHeats(category.categoryId);
        }
    }

    setSelectedHeats(categoryId:number) {
        this.selectedHeats = this.surfEvent.heats.filter(heat => {
            return heat.categoryId == categoryId;
        });
    }

    onSelecteHeat(heat:Heat) {
        this.selectedHeat = heat;
    }

    onSelectAvailableAthlete(athlete:Athlete) {
        this.selectedAvailableAthlete = athlete;
    }

    onSelectAssignedAthlete(athlete:Athlete) {
        this.selectAssignedAthlete = athlete;
    }

    assignSelectedCategory() {
        if (!this.selectedAvailableCategory) {
            return;
        }

        var category = new EventCategory(this.surfEvent.surfEventId, this.selectedAvailableCategory.categoryId);

        category.categoryId = this.selectedAvailableCategory.categoryId;
        category.category = this.selectedAvailableCategory;
        category.athletes = [];

        this.surfEvent.categories.push(category);
        this.availablesCategories.splice(this.availablesCategories.indexOf(this.selectedAvailableCategory), 1);

    }

    assignAllCategories() {
        this.availablesCategories.forEach(cat => {
            var category = new EventCategory(this.surfEvent.surfEventId, cat.categoryId);
            category.category = cat;

            this.surfEvent.categories.push(category);
        });

        this.setAvaileableCategories(this._allCategories);
    }

    unAssignSelectedCategory() {
        if (!this.selectedAssignedCategory) {
            return;
        }
        let self = this;
        this.availablesCategories.push(this.selectedAssignedCategory);
        this.surfEvent.categories = this.surfEvent.categories.filter(eventCat => {
            if (eventCat.category.categoryId === self.selectedAssignedCategory.categoryId) {
                return false;
            }
            return true;
        });
        this.setAvaileableCategories(this._allCategories);
    }

    unAssignAllCategories() {
        this.surfEvent.categories.map(cat => {
            this.availablesCategories.push(cat.category);
        });

        this.surfEvent.categories = [];
        this.setAvaileableCategories(this._allCategories);

    }

    unAssingSelectedAthlete() {
        if (!this.selectAssignedAthlete) {
            return;
        }

        let categoryAthlete = new EventCategoryAthlete();
        categoryAthlete.athleteId = this.selectAssignedAthlete.athleteId;
        categoryAthlete.eventCategoryId = this.selectedEventCategory.eventCategoryId;
        categoryAthlete.category = this.selectedEventCategory;
        categoryAthlete.athlete = this.selectAssignedAthlete;

        this.selectedEventCategory.athletes
            .splice(this.selectedEventCategory.athletes.indexOf(categoryAthlete), 1);

        this.setAvaileableAthletes(this._allAthletes);
        this.selectedAvailableAthlete = null;
        this.selectAssignedAthlete = null;
    }

    assignSelectedAthlete() {
        if (!this.selectedEventCategory) {
            return;
        }

        if (!this.selectedAvailableAthlete) {
            return;
        }

        let categoryAthlete = new EventCategoryAthlete();
        categoryAthlete.athleteId = this.selectedAvailableAthlete.athleteId;
        categoryAthlete.eventCategoryId = this.selectedEventCategory.eventCategoryId;
        // categoryAthlete.category = this.selectedEventCategory;
        categoryAthlete.athlete = this.selectedAvailableAthlete;

        this.selectedEventCategory.athletes.push(categoryAthlete);
        this.setAvaileableAthletes(this._allAthletes);

        this.selectedAvailableAthlete = null;
        this.selectAssignedAthlete = null;
    }

    unAssignAllAthletes() {
        this.selectedEventCategory.athletes = [];
        this.setAvaileableAthletes(this._allAthletes);

        this.selectedAvailableAthlete = null;
        this.selectAssignedAthlete = null;
    }

    assignAllAthletes() {
        if (!this.selectedEventCategory) {
            return;
        }
        this.availableAthletes.forEach(athlete => {
            let categoryAthlete = new EventCategoryAthlete();
            categoryAthlete.athleteId = athlete.athleteId;
            categoryAthlete.eventCategoryId = this.selectedEventCategory.eventCategoryId;
            // categoryAthlete.category = this.selectedEventCategory;
            categoryAthlete.athlete = athlete;

            this.selectedEventCategory.athletes.push(categoryAthlete);
        });

        this.setAvaileableAthletes(this._allAthletes);
    }


    BuildHeats() {
        this._heatService.buildHeat(this.surfEvent.categories)
            .subscribe(data => {
                this.surfEvent.heats = data;
                console.log(JSON.stringify(data));
            });
    }

    getAthleteColor(colorCode:number) {
        return this._systemInfo.getAthleteExaColor(colorCode);
    }

    onSave(event:SurfEvent) {
        let self = this;
        if (!event.surfEventId ||
            event.surfEventId === 0) {
            self._eventService.add(this.surfEvent)
                .subscribe(resp => {
                    this._systemInfo.success('Event created successfully.');
                    this._router.navigate(['Events']);
                });
            return;
        }

        this._eventService.edit(this.surfEvent)
            .subscribe(resp => {
                self._systemInfo.success('Event updated successfully.');
            });
    }

    getEventStatus() {
        return this._systemInfo.getEventStatus();
    }

    initilizeDatePiker() {
        let event = this.surfEvent;
        if ($ && $().daterangepicker) {
            $('#start').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                timePicker: true,
                timePickerIncrement: 30,
                showDropdowns: true,
                locale: {
                    format: 'MM/DD/YYYY h:mm A'
                }
            }, function (start /*, end, label*/) {
                event.start = start.format('DD/MM/YYYY HH:mm');
                // console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#end').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
                timePicker: true,
                timePickerIncrement: 30,
                showDropdowns: true,
                locale: {
                    format: 'MM/DD/YYYY h:mm A'
                }
            }, function (start /*, end, label*/) {
                event.end = start.format('DD/MM/YYYY HH:mm');
                // console.log(start.toISOString(), end.toISOString(), label);
            });
        }
        else {
            console.error('jQuery.datepicker not defined.');
        }
    }

    initializeWizard() {
        let self = this;
        let router = self._router;
        if (!$ && !$().smartWizard) {
            console.error('jQuery.smartWizard not defined.');
            return;
        }

        $('#wizard').smartWizard({
            includeFinishButton: self.surfEvent.status === 1, //Add the finish button
            // events
            onFinish: function () {
                self.onSave(self.surfEvent);
            }
        });

        let cancelLink = document.createElement('a');
        cancelLink.text = 'Cancel';
        cancelLink.className = 'btn btn-danger';
        cancelLink.onclick = function () {
            router.navigate(['Events']);
        }

        $('.btn').removeAttr("href");
        $('.actionBar').prepend(cancelLink);
    }
}
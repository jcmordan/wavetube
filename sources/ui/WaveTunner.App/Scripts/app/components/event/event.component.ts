import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';

import {JqueryDatatableDirective}            from '../../directives/jquery-datatable.directive';
import {JqueryDataTableEvent} from '../../directives/jquery-datable-event';

import {SurftEventService} from '../../services/surf_event.service'
import {SurfEvent} from '../../core/surf_event'


@Component({
    selector: 'athletes-content',
    templateUrl: 'Scripts/app/components/event/event.component.html',
    directives: [ROUTER_DIRECTIVES, JqueryDatatableDirective]
})

export class EventComponent implements OnInit {
    public surfEvents:SurfEvent[];
    public isLoading:boolean = true;
    public dataTableOptions:any;
    public dataTableEvents:JqueryDataTableEvent[] = [];

    constructor(private _router: Router,
                private _eventService: SurftEventService
    ) {}

    ngOnInit() {
        this.getAthletes();
    }

    getAthletes() {
        this.isLoading = true;
        this._eventService.getEvents()
            .subscribe(result => {
                this.surfEvents = result;
                this.initializeDatatable(this.surfEvents);
                this.isLoading = false;
            });
    }

    addNew() {
        this._router.navigate(['EventCreate']);
    }

    initializeDatatable(surfEvents:SurfEvent[]) {
        if (surfEvents.length === 0) {
            this.surfEvents = [];
            this.dataTableOptions = {};
            return;
        }

        var items = [];

        for (let c in surfEvents) {
            items.push({
                name: surfEvents[c].name,
                description: surfEvents[c].description,
                place: surfEvents[c].place,
                start: surfEvents[c].start,
                end: surfEvents[c].end,
                status: surfEvents[c].statusDescription,
                id: surfEvents[c].surfEventId,
            });
        }

        this.dataTableOptions = {
            "data": items,
            // "autoWidth"    : false,
            "columns": Object.keys(items[0]).map(columnName => {
                return {'data': columnName};
            }),
            "columnDefs": [
                {
                    "targets": -1,

                    render: function (data, type, event, meta) {
                        return `<td>
		            				<div class="row">		            					
										<div class="col-lg-12">
										    <div class="col-lg-4" >
											    <a data-id="${event.id}" class="fa fa-pencil edit-link" data-toggle="tooltip" data-original-title="Action">
											</a>
											</div>
											<div class="col-lg-4 ${event.status !== 'Pending' ? 'hidden' : ''}">
											    <a data-id="${event.id}" class="fa fa-play open-link" data-toggle="tooltip" data-original-title="Action">
											</a>
											</div>
										</div>									
				            		</div>
		            			</td>`;
                    },
                },
            ]
        };

        this.defineDataTableEvent();
    }

    defineDataTableEvent() {
        let router = this._router;
        let service = this._eventService;

        this.dataTableEvents.push({
            eventName: 'click',
            selector: '.edit-link',
            callback: function () {
                router.navigate(['EventEdit', {id: + this.attributes['data-id'].value}]);
            }
        });

        this.dataTableEvents.push({
            eventName: 'click',
            selector: '.open-link',
            callback: function () {
                if(!confirm("Are you sould you want open this events?")){
                    return;
                }


                router.navigate(['EventEdit', {id: + this.attributes['data-id'].value}]);
            }
        });
    }
}

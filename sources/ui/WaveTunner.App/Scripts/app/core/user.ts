import {IdentificationType} from "./identification-type";
import {Rol} from "./rol";

export class User {
    public userId:number;
    public userName:string;
    public password:string;
    public firstName:string;
    public lastName:string;
    public dateOfBirth: Date;
    public identificationType:IdentificationType;
    public identificationNumber:string;
    public rol:number;
    public rolDescription:string;
    public email:string;
    public active:boolean;
}




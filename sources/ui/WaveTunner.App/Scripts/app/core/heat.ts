import {SurfEvent} from "./surf_event";
import {Category} from "./category";
import {HeatAthlete} from "./heat_athlete";
import {HeatScore} from "./heat_score";
import {HeatAthletePenalty} from "./heat_athlete_penalty";
export class Heat{
    heatId: number;
    round: string;
    number: number;
    duration: number;
    surfEventId: number;
    categoryId: number;
    opendDate: Date;
    closeDate: Date;
    status: number;
    
    surfEvent: SurfEvent;
    category: Category;
    athletes: HeatAthlete[];
    scores: HeatScore[];
    penalties : HeatAthletePenalty [];
}
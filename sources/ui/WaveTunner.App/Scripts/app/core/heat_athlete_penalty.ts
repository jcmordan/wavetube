/**
 * Created by LuisJavier on 7/31/2016.
 */
import {Heat} from "./heat";
import {Athlete} from "./athlete";
import {Judge} from "./judge";

export class HeatAthletePenalty {
    heatAthletePenaltyId: number;
    heatId: number;
    athleteId: number;
    judgeId: number;
    type : number;

    heat: Heat;
    athlete: Athlete;
    judge: Judge;
}
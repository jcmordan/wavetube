import {Heat} from "./heat";
import {Athlete} from "./athlete";
export class  HeatAthlete{
    heatAthleteId : number;
    heatId: number;
    athleteId: number;
    colorCode: number;

    heat: Heat;
    athlete: Athlete;
}
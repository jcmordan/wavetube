
import { IdentificationType } from './identification-type'

export class Judge{
    public judgeId: number;
    public firstName: string;
    public lastName: string;
    public dateOfBirth: Date;
    public identificationType : IdentificationType;
    public identificationNumber: string;
    public email: string;
    public active: boolean;
}
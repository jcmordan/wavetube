import {Heat} from "./heat";
import {Athlete} from "./athlete";
import {Judge} from "./judge";

export class HeatScore{
    heatScoreId: number;
    heatId: number;
    athleteId: number;
    judgeId: number;
    score: number;

    heat: Heat;
    athlete: Athlete;
    judge: Judge;
}

import {Category} from "./category";

export class  AthleteCategory{
    public athleteId: number;
    public categoryId: number;
    public initialScore: number;

    public category: Category;


    constructor(_athleteId:number, _categoryId:number){
        this.athleteId = _athleteId;
        this.categoryId = _categoryId;
    }
}
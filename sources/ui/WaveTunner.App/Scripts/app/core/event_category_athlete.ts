import {Athlete} from "./athlete";
import {EventCategory} from "./event_category";
export class EventCategoryAthlete{
    public eventCategoryId: number;
    public athleteId: number;

    public category: EventCategory;
    public athlete: Athlete;
}
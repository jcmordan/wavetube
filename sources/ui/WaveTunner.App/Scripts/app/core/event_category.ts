import {SurfEvent} from "./surf_event";
import {Category} from "./category";
import {Athlete} from "./athlete";
import {EventCategoryAthlete} from "./event_category_athlete";

export class EventCategory{
    public eventCategoryId: number;
    public surfEventId: number;
    public categoryId: number;

    public event: SurfEvent;
    public category: Category;
    public athletes: EventCategoryAthlete[];

    constructor(_surfEventId:number, categoryId: number){
        this.categoryId = categoryId;
        this.surfEventId = _surfEventId;
        this.athletes = [];
    }
}
import {EventStatus} from "./event_status";
import {EventCategory} from "./event_category";
import {Heat} from "./heat";

export class SurfEvent{
    public surfEventId: number;
    public name: string;
    public description: string;
    public comments: string;
    public start: Date;
    public end: Date;
    public place: string;
    public eventStatusId: number;
    public status: number;
    public statusDescription: string;

    public categories: EventCategory[];
    public heats: Heat[];
}

import { IdentificationType } from './identification-type';
import {AthleteCategory} from "./athlete_category";

export class Athlete{
    public athleteId: number;
    public photo: string;
    public firstName: string;
    public lastName: string;
    public fullName: string;
    public dateOfBirth: Date;
    public identificationType : IdentificationType;
    public identificationNumber: string;
    public email: string;
    public active: boolean;

    public categories: AthleteCategory[];


}
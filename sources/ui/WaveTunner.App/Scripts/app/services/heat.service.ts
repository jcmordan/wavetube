import { Injectable }from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable} from 'rxjs/Observable';
import {SystemInfo} from "../system_info";
import {EventCategory} from "../core/event_category";
import {Heat} from "../core/heat";
import {HeatScore} from "../core/heat_score";
import {HeatAthletePenalty} from "../core/heat_athlete_penalty";

@Injectable()
export class HeatService {
    private _heatUrl: string;

    constructor(
        private _http: Http,
        private _systemInfo: SystemInfo
    ){
        this._heatUrl = _systemInfo.baseUrl.concat('heats')
    }

    buildHeat(categories: EventCategory[]){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(`${this._heatUrl}/create`, JSON.stringify(categories), options)
            .map(resp => <Heat[]> resp.json())
            .catch(this._systemInfo.handleError);
    }

    getCurrentHeat(eventId: number){
        return this._http.get(`${this._heatUrl}/current?eventId=${eventId}`)
            .map(res => <Heat>res.json())
            .catch(this._systemInfo.handleError);
    }

    getPreviousHeat(eventId:number)
    {
        return this._http.get(`${this._heatUrl}/previous?eventId=${eventId}`)
            .map(res => <Heat>res.json())
            .catch(this._systemInfo.handleError);
    }

    getNextHeat(eventId:number, round:string, current:number)
    {
        return this._http.get(`${this._heatUrl}/next?eventId=${eventId}&round=${round}&currentHeatId=${current}`)
            .map(res => <Heat>res.json())
            .catch(this._systemInfo.handleError);
    }
    saveHeatScore (heatScore : HeatScore){
        return this._http.post(`${this._heatUrl}/score`,heatScore)
            .map(res => <Heat>res.json())
            .catch(this._systemInfo.handleError);
    }
    saveHeatAthletePenalty (heatAthletePenalty : HeatAthletePenalty ) {
        return this._http.post(`${this._heatUrl}/penalty`,heatAthletePenalty)
            .map(res => <Heat>res.json())
            .catch(this._systemInfo.handleError);

    }
}
import { Injectable }from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';

import { IdentificationType } from '../core/identification-type';
import {SystemInfo} from "../system_info";
import {EventStatus} from "../core/event_status";
import {Category} from "../core/category";



@Injectable()
export class GeneralService {
    private _userUrl: string;
    private _categoryUrl:string;
    private _systemUrl:string;


    constructor(
        private _http: Http,
        private _config: SystemInfo
    ){
        this._userUrl = _config.baseUrl.concat('users/');
        this._categoryUrl = _config.baseUrl.concat('categories/');
        this._systemUrl = _config.baseUrl.concat('system/');
    }

  getIdentifiacitonTypes(){
      var types : IdentificationType[] = [
          {"id": 1, "description": "Cedula"},
          {"id": 2, "description": "Pasaporte"}
      ];

      return Observable.of(types);
  }

    getEventStatus(){
       return this._http.get(this._config.baseUrl.concat('surfevents/eventstatus'))
            .map(resp => <EventStatus>resp.json())
            .catch(this._config.handleError);
    }

    getCategories(){
        return this._http.get(this._categoryUrl)
            .map(resp => <Category[]>resp.json())
            .catch(this._config.handleError);
    }

    getVersion(){
        return this._http.get(this._systemUrl.concat('version'))
            .map(resp => <string> resp.json())
            .catch(this._config.handleError);
    }
}

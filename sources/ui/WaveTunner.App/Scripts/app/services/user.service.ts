import { Injectable }from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import { User } from '../core/user';
import {SystemInfo} from "../system_info";



@Injectable()
export class UserService {
    private _userUrl: string;

    constructor(
        private _http: Http,
        private _cofig: SystemInfo
    ){
        this._userUrl = _cofig.baseUrl.concat('users/')
    }

    getUsers(){
        return this._http.get(this._userUrl)
            .map(res => <User[]>res.json())
            .catch(this.handleError);
    }

    getUser(userId:number){
        return this._http.get(`${this._userUrl}${userId}`)
            .map(res => <User>res.json())
            .catch(this.handleError);
    }

    add(user:User){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._userUrl, JSON.stringify(user), options)
            .catch(this.handleError);
    }

    edit(user:User){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.put(`${this._userUrl}/${user.userId}`, JSON.stringify(user), options)
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        // TODO: implement server error loggin
        if (error.status !== 404) {
            alert('Error! See console for details');
            console.error(error);
        }
        return Observable.throw(error || 'Server error');
    }
}

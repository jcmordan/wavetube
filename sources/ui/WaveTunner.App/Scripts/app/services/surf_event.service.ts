import { Injectable }from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable} from 'rxjs/Observable';

import  {SurfEvent} from "../core/surf_event";
import {SystemInfo} from "../system_info";

@Injectable()
export class SurftEventService{
    private _url: string ;

    constructor(
        private _http: Http,
        private _config: SystemInfo
    ){
        this._url = _config.baseUrl.concat('surfevents/')
    }

    getEvents(){
        return this._http.get(this._url)
            .map(resp => <SurfEvent[]>resp.json())
            .catch(this._config.handleError);
    }

    getEvent(id:number){
        return this._http.get(`${this._url}${id}`)
            .map(resp => <SurfEvent> resp.json())
            .catch(this._config.handleError);
    }

    add(event: SurfEvent){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._url, JSON.stringify(event), options)
            .catch(this._config.handleError);
    }

    edit(event: SurfEvent){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.put(`${this._url}${event.surfEventId}`, JSON.stringify(event), options)
            .catch(this._config.handleError);
    }

    open(id: number){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.put(`${this._url}${id}/open`, null, options)
            .catch(this._config.handleError);
    }


    getNexEvents(count: number){
        return this._http.get(`${this._url}?status=1&count=${count}`)
            .map(resp => <SurfEvent[]>resp.json())
            .catch(this._config.handleError);
    }
}
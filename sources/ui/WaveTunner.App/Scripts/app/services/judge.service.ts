import { Injectable }from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable} from 'rxjs/Observable';

import {Judge} from "../core/judge";
import {SystemInfo} from "../system_info";

@Injectable()
export class JudgeServie{
    private _judgeUrl: string;

    constructor(
        private _http: Http,
        private _cofig: SystemInfo
    ){
        this._judgeUrl = _cofig.baseUrl.concat('judges/')
    }

    getJudges(){
        return this._http.get(this._judgeUrl)
            .map(res => <Judge[]>res.json())
            .catch(this.handleError);
    }

    getJudge(judgeId : number){
        return this._http.get(this._judgeUrl.concat(judgeId.toString()))
            .map(res => <Judge>res.json())
            .catch(this.handleError);
    }

    addJudge(judge: Judge){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._judgeUrl, JSON.stringify(judge), options)
            .catch(this.handleError);
    }

    updateJudge(judge: Judge){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.put(`${this._judgeUrl}${judge.judgeId}`, JSON.stringify(judge), options)

            .catch(this.handleError);
    }

    private handleError(error: Response) {
        // TODO: implement server error loggin
        if (error.status !== 404) {
            alert('Error! See console for details');
            console.error(error);
        }
        return Observable.throw(error || 'Server error');
    }
}
import { Injectable }from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable} from 'rxjs/Observable';
import {SystemInfo} from "../system_info";

import { Athlete } from '../core/athlete'


@Injectable()
export class AthleteService {
    private _atlhleteUrl: string;

    constructor(
        private _http: Http,
        private _cofig: SystemInfo
    ){
        this._atlhleteUrl = _cofig.baseUrl.concat('athletes/')
    }

    getAthletes(){
        return this._http.get(this._atlhleteUrl)
            .map(resp => <Athlete[]>resp.json())
            .catch(this.handleError);
    }

    getAthlete(athleteId: number){
        return this._http.get(this._atlhleteUrl + athleteId.toString())
            .map(resp => <Athlete>resp.json())
            .catch(this.handleError);
    }

    addAthlete(athlete: Athlete){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this._atlhleteUrl, JSON.stringify(athlete), options)
            .catch(this.handleError);
    }

    updateAthlete(athlete: Athlete){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.put(`${this._atlhleteUrl}${athlete.athleteId}`, JSON.stringify(athlete), options)
            .catch(this.handleError);
    }


    private handleError(error: Response) {
        // TODO: implement server error loggin
        if (error.status !== 404) {
            alert('Error! See console for details');
            console.error(error);
        }
        return Observable.throw(error || 'Server error');
    }
}
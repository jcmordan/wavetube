// function to act as a class
export class NumericCellEditor {
    eInput:HTMLInputElement;
    cancelBeforeStart:boolean;
    maxValue:number;
    minValue:number;

    init(params) {
        // create the cell
        this.eInput = document.createElement('input');
        this.eInput.type = 'number';
        this.eInput.value = this.isCharNumeric(params.charPress) ? params.charPress : params.value;

        var that = this;
        this.eInput.addEventListener('keypress', function (event) {
            if (!that.isKeyPressedNumeric(event)) {
                that.eInput.focus();
                if (event.preventDefault) event.preventDefault();
            }
        });

        // only start edit if key pressed is a number, not a letter
        this.cancelBeforeStart = params.charPress && ('1234567890.'.indexOf(params.charPress) < 0);

        this.maxValue = params.column.colDef.maxValue ? params.column.colDef.maxValue : 10;
        this.minValue = params.column.colDef.minValue ? params.column.colDef.minValue : 0;
    }

    getGui() {
        return this.eInput;
    }

    afterGuiAttached() {
        this.eInput.focus();
    }

    isCancelBeforeStart() {
        return this.cancelBeforeStart;
    }

    isCancelAfterEnd() {
        var value = +this.getValue();
        return value < this.minValue || value > this.maxValue;
    }

    getValue() {
        if(this.eInput.value.indexOf('.') == 0){
            this.eInput.value = 0 +this.eInput.value;
        }
        return this.eInput.value;
    }

    // any cleanup we need to be done here
    destroy() {
        // but this example is simple, no cleanup, we could  even leave this method out as it's optional
    }

    isPopup = function () {
        // and we could leave this method out also, false is the default
        return false;
    }

    private isCharNumeric(charStr) {
        return !!/^\d*\.?\d*$/.test(charStr);
    }

    private isKeyPressedNumeric(event) {
        var charCode = this.getCharCodeFromEvent(event);
        var charStr = String.fromCharCode(charCode);
        var value = this.getValue() + charStr;
        return this.isCharNumeric(value);
    }

    private getCharCodeFromEvent(event) {
        event = event || window.event;
        return (typeof event.which == "undefined") ? event.keyCode : event.which;
    }
}
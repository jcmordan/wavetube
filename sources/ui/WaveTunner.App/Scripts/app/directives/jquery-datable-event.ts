export class JqueryDataTableEvent{
    public eventName: string;
    public selector: string;
    public callback: Function;
}
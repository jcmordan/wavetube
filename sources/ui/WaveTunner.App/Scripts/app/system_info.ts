import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Rol} from "./core/rol";
import {EventStatus} from "./core/event_status";

declare var System : any;
var toastr : any;

System.import('toastr').then( _toastr => {
    toastr = _toastr;
}, console.error.bind(console));

export class SystemInfo{
    public baseUrl: string ='http://localhost:5001/api/';
    public nexEventsCount: number = 5;
    public  monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December' ];

    constructor() {
        toastr.options = {
            "positionClass"   : "toast-top-right",
            "closeButton"     : true,
            "timeOut"         : 3000,
            "extendedTimeOut" : 6000,
        };
    }

    public handleError(error: Response) {
        // TODO: implement server error loggin
        if (error.status !== 404) {
            alert('Error! See console for details');
            console.error(error);
        }
        return Observable.throw(error || 'Server error');
    }

    public dynamicSort(array: any[] , property: string) {
        function order (a: any , b: any) {
            if(a[property] < b[property]) return -1;
            if(a[property] > b[property]) return 1;
            return 0;
        }

        return array.sort(order);
    }

    success(message: string){
        toastr.success(message);
    }

    getAthleteExaColor(colorCode: number){
        let colors:string[] = ['#ff0000', '#ffffff', '#000000', '#ffff00'];
        return colors[colorCode - 1];
    }

    getAthleteColorName(colorCode: number){
        let colors:string[] = ['red', 'white', 'black', 'yellow'];
        return colors[colorCode - 1];
    }

    getUserRoles(){
        let roles: Rol[];
        roles = [
            {id: 1, description: 'Administrator'},
            {id: 2, description: 'User'},
            {id: 3, description: 'Judge'}
        ];

        return Observable.of(roles);s
    }

    getEventStatus(){
        let status: EventStatus[];
        status = [
            {id: 1, description: 'Pending'},
            {id: 2, description: 'Opened'},
            {id: 3, description: 'Closed'},
            {id: 4, description: 'Canceled'}
        ];
        return Observable.of(status);
    }
} 
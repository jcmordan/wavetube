import {Component} from '@angular/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import {HTTP_PROVIDERS}                      from '@angular/http';
import {Location} from '@angular/common';

import {HeroService} from './services/hero.service';
import {UserService} from './services/user.service';
import {AthleteService} from './services/athlete.service';
import {GeneralService} from './services/general.service';
import {JudgeServie} from "./services/judge.service";


import {DashboardComponent} from './components/dashboard/dashboard.component';
import {HeroesComponent} from './components/hero/heroes.component';
import {HeroDetailComponent} from './components/hero/widgets/hero-detail.component';
import {UserComponent} from './components/user/user.component'
import {AthleteComponent} from './components/athlete/atlete.component'
import {AthleteEditComponent} from './components/athlete/widgets/athlete-edit.component'

import {EventComponent} from "./components/event/event.component";
import {SystemInfo} from "./system_info";
import {SurftEventService} from "./services/surf_event.service";
import {EventEditorComponent} from "./components/event/widgets/event_edit.component";
import {HeatService} from "./services/heat.service";
import {HeatManager} from "./components/heat/heat_manager.component";
import {HeatJudgments} from "./components/heat/heat_judgments.component";
import {HeatOverview} from "./components/heat/heat_overview.component";
import {UserEditComponent} from "./components/user/widgets/user_edit.component";


@Component({
    selector: 'my-app',
    templateUrl: 'Scripts/app/app.component.html',
    styleUrls: ['Scripts/app/app.component.css'],
    directives: [ROUTER_DIRECTIVES],
    providers: [
        ROUTER_PROVIDERS,
        HTTP_PROVIDERS,
        HeroService,
        UserService,
        AthleteService,
        GeneralService,
        JudgeServie,
        SystemInfo,
        SurftEventService,
        HeatService
    ]
})
@RouteConfig([
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: DashboardComponent,
        useAsDefault: true
    },
    {
        path: '/heroes/detail/:id',
        name: 'HeroDetail',
        component: HeroDetailComponent
    },
    {
        path: '/heroes',
        name: 'Heroes',
        component: HeroesComponent
    },

    // user related
    {
        path: '/users',
        name: 'Users',
        component: UserComponent
    },
    {
        path: '/users/:id',
        name: 'UserEdit',
        component: UserEditComponent
    },
    {
        path: '/users/new',
        name: 'UserCreate',
        component: UserEditComponent
    },

    // athlete reated
    {
        path: '/athletes',
        name: 'Athletes',
        component: AthleteComponent
    },
    {
        path: '/athletes/:id',
        component: AthleteEditComponent,
        name: 'AthleteEdit',
    },
    {
        path: '/athletes/new',
        component: AthleteEditComponent,
        name: 'AthleteCreate',
    },


    // events related
    {
        path: '/events',
        component: EventComponent,
        name: 'Events'
    },
    {
        path: '/events/:id',
        component: EventEditorComponent,
        name: 'EventEdit'
    },
    {
        path: '/events/new',
        component: EventEditorComponent,
        name: 'EventCreate'
    },
    // heats realated
    {
        path: '/heats',
        component: HeatManager,
        name: 'Heats'
    },
    {
        path: '/heats/manager',
        component: HeatManager,
        name: 'HeatManager'
    },
    {
        path: '/heats/judgments',
        component: HeatJudgments,
        name: 'HeatJudgments'
    },
    {
        path: '/heats/overview',
        component: HeatOverview,
        name: 'HeatOverview'
    }
])

export class AppComponent {
    title = 'Tour of Heroes';
    version: string = '';
    activeUrl:string;

    constructor(
        private _router:Router,
        private _location:Location,
        private _generalService: GeneralService
    ) {
        this._router.subscribe(url => {
            this.activeUrl = url;
        });
        this._generalService.getVersion()
            .subscribe(result => {
                this.version = result;
            });
    }

    isRouteActive(routeName:string):boolean {
        return this._router.isRouteActive(this._router.generate([routeName]));
    }


}

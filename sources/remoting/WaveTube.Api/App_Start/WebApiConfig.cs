﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace WaveTube.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Formatting

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var serializerSettings =
                config
                    .Formatters
                    .JsonFormatter
                    .SerializerSettings;

            serializerSettings.DateFormatString = "MM-dd-yyyy hh:mm:ss";

            serializerSettings
                .ContractResolver = new CamelCasePropertyNamesContractResolver();

            serializerSettings
                .Formatting = Newtonsoft.Json.Formatting.Indented;

            serializerSettings
                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;


        }
    }
}

﻿using System.Collections.Generic;
using System.Web.Http;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Core;

namespace WaveTube.Api.Controllers
{
    public class AthletesController : BaseController<Athlete>
    {
        private readonly IAthleteRules _rules;
        public AthletesController(IAthleteRules rules)      
        {
            _rules = rules;
        }

        // GET: api/Athletes
        public virtual IEnumerable<Athlete> Get()
            => _rules.GetAll(GetAuthenticationContext());

        // GET: api/Athletes/5
        public virtual Athlete Get(int id)
            => _rules.Get(id, GetAuthenticationContext());

        // POST: api/Athletes
        public virtual void Post([FromBody]Athlete value)
            => _rules.Add(value, GetAuthenticationContext());

        // PUT: api/Athletes/5
        public virtual void Put(int id, [FromBody]Athlete value)
            => _rules.Modify(value, GetAuthenticationContext());
    }
}

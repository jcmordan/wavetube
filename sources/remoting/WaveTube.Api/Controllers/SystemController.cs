﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace WaveTube.Api.Controllers
{
    public class SystemController : ApiController
    {
        // GET: api/system/version
        [HttpGet]
        [Route("api/system/version")]
        public string Version()
        {

            Assembly assembly = Assembly.GetExecutingAssembly();
            string version = assembly.GetName().Version.ToString();
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(assembly.Location);
            DateTime lastModified = fileInfo.LastWriteTime;

            return $"last update @ {lastModified} ";
        }

    }
}

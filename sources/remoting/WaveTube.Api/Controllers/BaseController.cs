﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Application;

namespace WaveTube.Api.Controllers
{
    public class BaseController<T> : ApiController
        where T: class
    {
        protected AuthenticationContext GetAuthenticationContext()
        {
            var user = new AuthenticationContext();
            var request = HttpContext.Current.Request;

            if (request.Headers.Count == 0)
            {
                return user;
            }

            var auth = request.Headers["Authorization"]; 
            if (string.IsNullOrEmpty(auth))
            {
                return user;
            }

            var cred = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(auth.Substring(6))).Split(':');
            user.Token = cred[1];

            var username = cred[0];

            if (username.Contains("@"))
            {
                user.Username = username.Substring(username.IndexOf("@"));
                user.Client = username.Substring(0, username.IndexOf("@"));
            }
            else {
                user.Username = username;
            }

            return user;
        }
    }
}

﻿using System.Collections.Generic;
using System.Web.Http;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Core;

namespace WaveTube.Api.Controllers
{
    public class UsersController : BaseController<User>
    {
        private readonly IUserRules _rules;
        public UsersController(IUserRules rules)          
        {
            _rules = rules;
        }

        // GET: api/Users
        public virtual IEnumerable<User> Get()
            => _rules.GetAll(GetAuthenticationContext());

        // GET: api/Users/5
        public virtual User Get(int id)
            => _rules.Get(id, GetAuthenticationContext());

        // POST: api/Users
        public virtual void Post([FromBody]User value)
            => _rules.Add(value, GetAuthenticationContext());

        // PUT: api/Users/5
        public virtual void Put(int id, [FromBody]User value)
            => _rules.Modify(value, GetAuthenticationContext());
    }
}

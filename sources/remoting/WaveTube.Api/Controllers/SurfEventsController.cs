﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Core;

namespace WaveTube.Api.Controllers
{
    public class SurfEventsController : BaseController<SurfEvent>
    {
        private readonly ISurfEventRules _rules;
        public SurfEventsController(ISurfEventRules rules)          
        {
            _rules = rules;
        }

        // GET: api/SurfEvents
        public virtual IEnumerable<SurfEvent> Get()
            => _rules.GetAll(GetAuthenticationContext());

        // GET: api/SurfEvents/5
        public virtual SurfEvent Get(int id)
            => _rules.Get(id, GetAuthenticationContext());

        // POST: api/SurfEvents
        public virtual void Post([FromBody]SurfEvent value)
            => _rules.Add(value, GetAuthenticationContext());

        // PUT: api/CategorySurfEvents/5
        public virtual void Put(int id, [FromBody]SurfEvent value)
            => _rules.Modify(value, GetAuthenticationContext());

        [HttpGet]
        public IEnumerable<SurfEvent> Get(int? status, int? count)
            => _rules.GetAll(status, count);

        // put: api/surfevents/1/open
        [HttpPut]
        [Route("api/SurfEvents/{id}/open")]
        public HttpResponseMessage Open(int id)
        {
            try
            {
                SurfEvent surfEvent = _rules.OpenHeat(id);

                return Request.CreateResponse(HttpStatusCode.OK,
                    new { Success = true, Message = "OK", surfEvent = surfEvent });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Success = false, Message = ex.Message });
            }
        }
    }
}

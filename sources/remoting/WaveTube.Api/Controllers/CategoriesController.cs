﻿using System.Collections.Generic;
using System.Web.Http;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Core;

namespace WaveTube.Api.Controllers
{
    public class CategoriesController : BaseController<Category>
    {
        private readonly ICategoryRules _rules;
        public CategoriesController(ICategoryRules rules) 
        {
            _rules = rules;
        }

        // GET: api/Categories
        public virtual IEnumerable<Category> Get()
            => _rules.GetAll(GetAuthenticationContext());

        // GET: api/Categories/5
        public virtual Category Get(int id)
            => _rules.Get(id, GetAuthenticationContext());

        // POST: api/Categories
        public virtual void Post([FromBody]Category value)
            => _rules.Add(value, GetAuthenticationContext());

        // PUT: api/Categories/5
        public virtual void Put(int id, [FromBody]Category value)
            => _rules.Modify(value, GetAuthenticationContext());
    }
}

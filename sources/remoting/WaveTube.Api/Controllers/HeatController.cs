﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WaveTube.Api.Filters;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Core.Rules;
using WaveTube.Models.Core;

namespace WaveTube.Api.Controllers
{
  
    public class HeatsController : BaseController<Heat>
    {
        private readonly IHeatRules _rules;
        public HeatsController(IHeatRules rules)             
        {
            _rules = rules;
        }

        // GET: api/Heats
        public virtual IEnumerable<Heat> Get()
            => _rules.GetAll(GetAuthenticationContext());

        // GET: api/Heats/5
        public virtual Heat Get(int id)
            => _rules.Get(id, GetAuthenticationContext());

        // POST: api/Heats
        public virtual void Post([FromBody]Heat value)
            => _rules.Add(value, GetAuthenticationContext());

        // PUT: api/Heats/5
        public virtual void Put(int id, [FromBody]Heat value)
            => _rules.Modify(value, GetAuthenticationContext());

        [HttpPost]
        [Route("api/heats/create")]
        public ICollection<Heat> BuildHeat([FromBody] ICollection<EventCategory> eventCategories) 
            => _rules.BuildHeats(eventCategories);

        [HttpGet]
        [Route("api/heats/current")]
        [NullObjectActionFilter]
        public Heat GetCurrent(int eventId)
           => _rules.GetCurrentHeat(eventId);

        [HttpGet]
        [Route("api/heats/previous")]
        [NullObjectActionFilter]
        public Heat GetPrevious(int eventId)
         => _rules.GetPreviousHeat(eventId);

        [HttpGet]
        [Route("api/heats/next")]
        [NullObjectActionFilter]
        public Heat GetNext(int eventId, string round, int currentHeatId)
            => _rules.GetNextHeat(eventId, round, currentHeatId);
        
        [HttpPost]
        [Route("api/heats/score")]
        [NullObjectActionFilter]
        public HttpResponseMessage SaveHeatScore(HeatScore heatScore)
        {
            _rules.SaveHeatScore(heatScore);
            return Request.CreateResponse(HttpStatusCode.Created,
                    new { Success = true, Message = "Created" });
        }
        [HttpPost]
        [Route("api/heats/penalty")]
        [NullObjectActionFilter]
        public HttpResponseMessage SaveHeatPenalty(HeatAthletePenalty heatAthletePenalty)
        {
            (_manager as IHeatRepository).SaveHeatPenalty(heatAthletePenalty);
            return Request.CreateResponse(HttpStatusCode.Created,
                    new { Success = true, Message = "Created" });
        }
    }
}

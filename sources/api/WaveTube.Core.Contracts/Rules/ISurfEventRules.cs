﻿using System.Collections.Generic;
using System.Net.Http;
using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Rules
{
    public interface ISurfEventRules : IRules<SurfEvent>
    {
        IEnumerable<SurfEvent> GetAll(int? status, int? count);       
        SurfEvent OpenHeat(int id);
    }
}

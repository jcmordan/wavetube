﻿using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Rules
{
    public interface IAthleteRules : IRules<Athlete>
    {
    }
}

﻿using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Rules
{
    public interface IPersonRules<T>: IRules<T>
        where T: Person
    {
    }
}

﻿using System.Collections.Generic;
using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Rules
{
    public interface IHeatRules : IRules<Heat>
    {
        ICollection<Heat> BuildHeats(ICollection<EventCategory> eventCategories);
        Heat GetCurrentHeat(int eventId);
        Heat GetPreviousHeat(int eventId);
        Heat GetNextHeat(int eventId, string round, int currentHeatId);
        void SaveHeatScore(HeatScore heatScore);
    }
}

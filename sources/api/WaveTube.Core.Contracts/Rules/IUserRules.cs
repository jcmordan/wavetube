﻿using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Rules
{
    public interface IUserRules : IRules<User>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using WaveTube.Models.Application;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Contracts.Rules
{
    public interface IRules<T> : IDisposable
        where T : class
    {
        ValidationException Validate(T model);

        bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext);

        T Get(int id, AuthenticationContext authContext);

        ICollection<T> GetAll(AuthenticationContext authContext);

        T Add(T model, AuthenticationContext authContext);

        T Modify(T model, AuthenticationContext authContext);
    }
}

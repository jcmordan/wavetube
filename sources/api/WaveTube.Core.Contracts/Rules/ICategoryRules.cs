﻿using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Rules
{
    public interface ICategoryRules : IRules<Category>
    {
    }
}

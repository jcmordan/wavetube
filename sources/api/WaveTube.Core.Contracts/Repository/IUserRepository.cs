﻿using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Repository
{
    public interface IUserRepository : IRepository<User>
    {
    }
}

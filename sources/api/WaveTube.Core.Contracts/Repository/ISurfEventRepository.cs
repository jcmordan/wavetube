﻿using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Repository
{
    public interface ISurfEventRepository : IRepository<SurfEvent>
    {
       
    }
}

﻿using System.Collections.Generic;
using WaveTube.Models.Core;

namespace WaveTube.Core.Contracts.Repository
{
    public interface IHeatRepository : IRepository<Heat>
    {
      
        Heat GetCurrentHeat(int eventId);
        Heat GetNextHeat(int eventId, string round, int currentHeatId);
        Heat GetPreviousHeat(int eventId);
        void SaveHeatScore(HeatScore heatScore);
        void SaveHeatPenalty(HeatAthletePenalty heatAthletePenalty);


    }
}

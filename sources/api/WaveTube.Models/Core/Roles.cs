﻿namespace WaveTube.Models.Core
{
    public enum Roles
    {
        Administrator = 1,
        RegularUser = 2,
        Judge = 3
    }
}

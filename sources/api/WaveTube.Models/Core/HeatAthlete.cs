﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{
    public enum AthelteColor
    {
        Color1 = 1,
        Color2 = 2,
        Color3 = 3,
        Color4 = 4
    }

    public class HeatAthlete
    {
        public int HeatAthleteId { get; set; }
        public int HeatId { get; set; }
        public int AthleteId { get; set; }
        public AthelteColor ColorCode { get; set; }

        public virtual Heat Heat { get; set; }
        public virtual Athlete Athlete { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using WaveTube.Models.Application;

namespace WaveTube.Models.Core
{
    public class SurfEvent
    {
        public SurfEvent() {
            Categories = new List<EventCategory>();
            Heats = new List<Heat>();
            Judges = new List<User>();
        }

        public int SurfEventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Place { get; set; }
        public EventStatus Status{ get; set; }
       
        public virtual ICollection<EventCategory> Categories { get; set; }
        public virtual ICollection<User> Judges { get; set; }
        public virtual ICollection<Heat> Heats { get; set; }

        public string StatusDescription
        {
            get
            {
                return Status.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveTube.Models.Application;

namespace WaveTube.Models.Core
{
    public class HeatAthletePenalty
    {
        public int HeatAthletePenaltyId { get; set; }
        public int HeatId { get; set; }
        public int JudgeId { get; set; }
        public int AthleteId { get; set; }
        public PenaltyType Type { get; set; }

        public virtual Heat Heat { get; set; }
        public virtual User Judge { get; set; }
        public virtual Athlete Athelte { get; set; }
    }
}

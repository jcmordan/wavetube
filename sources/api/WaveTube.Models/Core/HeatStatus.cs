﻿namespace WaveTube.Models.Core
{
    public enum HeatStatus
    {
        Pending = 1,
        Open = 2,
        Closed = 3
    }
}

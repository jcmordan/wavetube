﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{
    public class EventCategoryAthlete
    {
        public int EventCategoryAthleteId { get; set; }

        public int EventCategoryId { get; set; }
        public int AthleteId { get; set; }

        public virtual EventCategory EventCategory { get; set; }
        public virtual Athlete Athlete{ get; set; }
    }
}

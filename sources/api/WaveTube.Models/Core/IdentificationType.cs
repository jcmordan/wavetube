﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{   public class IdentificationType
    {
        public int IdentificationTypeId { get;set;}
        public string Description { get; set; }
    }
}

﻿namespace WaveTube.Models.Core
{
    public class HeatScore
    {
        public int HeatScoreId { get; set; }
        public int HeatId { get; set; }
        public int JudgeId { get; set; }
        public int AthleteId { get; set; }
        public double Score { get; set; }

        public virtual Heat Heat { get; set; }
        public virtual User Judge { get; set; }
        public virtual Athlete Athelte { get; set; }
    }
}

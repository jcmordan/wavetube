﻿namespace WaveTube.Models.Core
{
    public class User : Person
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Roles Rol { get; set; }
        public string RolDescription => Rol.ToString();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{
    public class EventCategory
    {
        public EventCategory()
        {
            this.Athletes = new List<EventCategoryAthlete>();
        }

        public int EventCategoryId {get;set;}
        public int CategoryId { get; set; }
        public int SurfEventId { get; set; }

        public virtual SurfEvent Event { get; set; }
        public virtual Category Category { get; set; }
        public ICollection<EventCategoryAthlete> Athletes { get; set; }
    }
}

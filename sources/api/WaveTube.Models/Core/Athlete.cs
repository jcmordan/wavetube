﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{
    public class Athlete : Person
    {
        public Athlete()
        {
            Scores = new List<HeatScore>();
            Categories = new List<AthleteCategory>();
        }

        public int AthleteId { get; set; }

        public virtual ICollection<AthleteCategory> Categories {get; set;}
        public virtual ICollection<HeatScore> Scores { get; set; }

        public double CurrentScore { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace WaveTube.Models.Core
{
    public class Heat
    {
        public int HeatId { get; set; }
        public string Round { get; set; }
        public int Number { get; set; }
        public int Duration { get; set; }
        public int SurfEventId {get;set;}
        public int CategoryId { get; set; }
        public HeatStatus Status { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }

        public virtual SurfEvent Event { get; set; }
        public virtual Category Category { get; set; }

        public virtual ICollection<HeatAthlete> Athletes { get; set; }
        public virtual ICollection<HeatScore> Scores { get; set; }
        public virtual ICollection<HeatAthletePenalty> Penalties { get; set; }
    }
}

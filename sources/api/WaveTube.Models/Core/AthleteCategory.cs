﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{
    public class AthleteCategory
    {
        public int AthleteId { get; set; }
        public int CategoryId { get; set; }
        public double InitialScore { get; set; }

        public virtual Athlete Athlete { get; set; }
        public virtual Category Category { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Core
{
    public class Category
    {
        public Category()
        {
            this.Athletes = new List<AthleteCategory>();
        }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }

        public virtual ICollection<AthleteCategory> Athletes { get; set; }
    }
}

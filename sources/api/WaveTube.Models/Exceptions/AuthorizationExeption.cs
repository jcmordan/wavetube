﻿using System;
using WaveTube.Models.Application;

namespace WaveTube.Models.Exceptions
{
    public class AuthorizationExeption: Exception
    {
        public AuthorizationError Error { get; set; }
        public AuthorizationExeption(AuthorizationError error, string message) 
            : base(message)
        {
            this.Error = error;
        }
    }
}

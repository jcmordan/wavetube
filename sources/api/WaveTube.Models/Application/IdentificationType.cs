﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Application
{

    public enum IdentificationType
    {
        Cedula = 1,
        Passport = 2
    }
}

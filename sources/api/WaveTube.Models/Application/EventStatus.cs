﻿namespace WaveTube.Models.Application
{
    public enum EventStatus
    {
        Pending = 1,
        Opened = 2,
        Closed = 3,
        Canceled = 4
    }
}

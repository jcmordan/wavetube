﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.Models.Application
{
    public enum ValidationError
    {
        
        Missing,
        
        Empty,
        
        NotEmpty,
        
        LargerThanExpected,
        
        ShorterThanExpected,
        
        InvalidFormat,
        
        InvalidCharacters,
        
        RangeInverted,
        
        RangeWithoutBoundaries,
        
        RangeInvalid,
        
        DataTypeDoesNotSupportValueRange,
        
        ChangeNotAllowed,
        
        Duplicated,
        
        NotFoundInRepository,
        
        NotAllowed,
        
        UnexpectedValue
    }





}

﻿namespace WaveTube.Models.Application
{
    public class AuthenticationContext
    {
        public string Username { get; set; }
        public string Token { get; set;}
        public string Client { get; set; }
    }
}

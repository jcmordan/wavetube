﻿using System.Data.Entity.ModelConfiguration;
using WaveTube.Models.Core;

namespace WaveTube.Data.EntityConfigurations
{
    public class AthleteConfiguration : EntityTypeConfiguration<Athlete>
    {
        public AthleteConfiguration() {
            this.HasMany<AthleteCategory>(ath => ath.Categories);

            this.Ignore(p => p.FullName);
            this.Ignore(a => a.CurrentScore);
        }
    }
}

﻿using System.Data.Entity.ModelConfiguration;
using WaveTube.Models.Core;

namespace WaveTube.Data.EntityConfigurations
{
    public class IdentificationTypeConfiguration : EntityTypeConfiguration<IdentificationType>
    {
        
        private const int MAX_NAME_LENGTH = 100;
        public IdentificationTypeConfiguration()
        {
            this.Property(ident => ident.Description)
                .HasMaxLength(MAX_NAME_LENGTH)
                .IsRequired();
        }
    }
}

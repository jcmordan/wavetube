﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveTube.Models;
using WaveTube.Models.Core;

namespace WaveTube.Data.EntityConfigurations
{
    public class SurfEventConfiguration : EntityTypeConfiguration<SurfEvent>
    {
   
        public SurfEventConfiguration()
        {
            this.Property(ev => ev.Name)
                .IsRequired();
            this.Property(ev => ev.Place)
                .IsRequired();
            this.Property(ev => ev.Start)
                .IsRequired();
            this.Property(ev => ev.End)
                .IsRequired();
            this.Property(ev => ev.Description)
                .IsRequired();

            this.Ignore(u => u.StatusDescription);
        }
    }
}

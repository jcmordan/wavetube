﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveTube.Models.Core;


namespace WaveTube.Data.EntityConfigurations
{
    public class EventCategoryConfiguration : EntityTypeConfiguration<EventCategory>
    {
        public EventCategoryConfiguration()
        {

            this.HasMany<EventCategoryAthlete>(ec => ec.Athletes)
                .WithRequired(ac => ac.EventCategory)
                .WillCascadeOnDelete(true);
        }
    }
}

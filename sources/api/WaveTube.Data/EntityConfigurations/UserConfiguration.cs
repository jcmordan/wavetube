﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveTube.Models;
using WaveTube.Models.Core;

namespace WaveTube.Data.EntityConfigurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {

        private const int MAX_USERNAME_LENGTH = 20;
        public UserConfiguration()
        {
            this.Property(u => u.UserName)
                .HasMaxLength(MAX_USERNAME_LENGTH)
                .IsRequired();


            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(MAX_USERNAME_LENGTH)
                .HasColumnAnnotation(
                IndexAnnotation.AnnotationName,
                new IndexAnnotation(
                    new System.ComponentModel.DataAnnotations.Schema.IndexAttribute("IX_UserName", 1) { IsUnique = true }));

            this.Ignore(u => u.RolDescription);
        }
    }
}

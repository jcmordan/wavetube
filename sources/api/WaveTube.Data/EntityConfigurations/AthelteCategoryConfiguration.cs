﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveTube.Models.Core;


namespace WaveTube.Data.EntityConfigurations
{
    public class AthelteCategoryConfiguration : EntityTypeConfiguration<AthleteCategory>
    {
        public AthelteCategoryConfiguration()
        {
            this.HasKey(ac => new { ac.AthleteId, ac.CategoryId });
        }
    }
}

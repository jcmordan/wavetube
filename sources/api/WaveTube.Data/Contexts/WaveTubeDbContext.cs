﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveTube.Data.Contracts;
using WaveTube.Data.EntityConfigurations;
using WaveTube.Models;
using WaveTube.Models.Core;


namespace WaveTube.Data.Contexts
{
    public class WaveTubeDbContext : DbContext, IDbContext
    {
        public WaveTubeDbContext() : base("WaveTubeDbContectionString")
        {

            // Database.SetInitializer<WaveTubeDbContext>(new CreateDatabaseIfNotExists<WaveTubeDbContext>());

            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = true;
            Configuration.AutoDetectChangesEnabled = true;
          
            //AutomaticMigrationsEnabled = false;
            //AutomaticMigrationDataLossAllowed = true;

        }

        #region DbSet

        public IDbSet<IdentificationType> IdentificationTypes { get; set; }
        public IDbSet<User> Users { get; set; }
        public IDbSet<Athlete> Athletes { get; set; }
    
        public IDbSet<SurfEvent> Events { get; set; }
        public IDbSet<Category> Categories { get; set; }
        public IDbSet<EventCategory> EventCategories { get; set; }
        public IDbSet<AthleteCategory> AthleteCategories { get; set; }
        public IDbSet<Heat> Heats { get; set; }
        public IDbSet<HeatAthlete> HeatAthletes { get; set; }
        public IDbSet<HeatScore> HeatAthleteScores { get; set; }

        #endregion        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Remove unused conventions
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Add entity configurations in a structured way using 'TypeConfiguration’ classes
      
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new SurfEventConfiguration());
            modelBuilder.Configurations.Add(new IdentificationTypeConfiguration());
            modelBuilder.Configurations.Add(new AthleteConfiguration());
            modelBuilder.Configurations.Add(new AthelteCategoryConfiguration());
            modelBuilder.Configurations.Add(new EventCategoryConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        #region IDbContext
        public string ConnectionString
        {
            get
            {
               return Database.Connection.ConnectionString;
            }

            set
            {
                Database.Connection.ConnectionString = value;
            }
        }

        public bool AutoDetectChangedEnabled
        {
            get
            {
                return Configuration.AutoDetectChangesEnabled;
            }

            set
            {
                Configuration.AutoDetectChangesEnabled = value;
            }
        }

        public void ExecuteSqlCommand(string sql, params object[] parameters)
        {
            Database.ExecuteSqlCommand(sql, parameters);
        }

        public void ExecuteSqlCommand(string sql)
        {
            Database.ExecuteSqlCommand(sql);
        }
        #endregion
    }
}

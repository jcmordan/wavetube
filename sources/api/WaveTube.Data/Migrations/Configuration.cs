namespace WaveTube.Data.Migrations
{
    using Models.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WaveTube.Data.Contexts.WaveTubeDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(WaveTube.Data.Contexts.WaveTubeDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var seedManager = new SeedManager();
            var types = new List<IdentificationType>{
                new IdentificationType {IdentificationTypeId=1, Description = "Cedula" },
                new IdentificationType {IdentificationTypeId=2, Description = "Pasaporte" },
                new IdentificationType {IdentificationTypeId=3, Description = "Otro" },
            };

            var categories = new List<Category>
            {
                new Category {CategoryId = 1, Description = "Body Board"},
                new Category {CategoryId = 2, Description = "Long Board"},
                new Category {CategoryId = 3, Description = "Junior"},
                new Category {CategoryId = 4, Description = "Women"},
            };

            foreach (var item in types)
            {
                context.IdentificationTypes.AddOrUpdate(item);
            }
            

            var persons = seedManager.GetPersons(100, types);          

            var rmd = new Random();
            var person = persons[rmd.Next(0, persons.Count - 1)];
            context.Users.AddOrUpdate(
                   new User
                   {
                       Password = "123456",
                       UserName = "manager",
                       DateOfBirth = person.DateOfBirth,
                       FirstName = person.FirstName,
                       IdentificationNumber = person.IdentificationNumber,
                       IdentificationTypeId = person.IdentificationTypeId,
                       Rol = Roles.Administrator,
                       LastName = person.LastName,
                       Email = person.Email,
                       Photo = person.Photo,
                       Active = rmd.Next(0, 1) == 0 ? false : true
                   }
               );

            foreach (var item in seedManager.GetAthletes(persons.ToList(), categories))
            {
                context.Athletes.AddOrUpdate(item);
            }

       


            foreach (var item in seedManager.GetEvents(10))
            {
                context.Events.AddOrUpdate(item);

            }

          

            foreach (var item in categories)
            {
                context.Categories.AddOrUpdate(item);
            }
        }
    }
}

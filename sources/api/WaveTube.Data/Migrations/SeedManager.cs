﻿using System;
using System.Collections.Generic;
using System.Linq;
using WaveTube.Models.Core;

namespace WaveTube.Data.Migrations
{
    public class SeedManager
    {
        private RandomDataGenerator.RandomDataGenerator _random;
        public SeedManager()
        {
            var path = @"C:\projects\wavetube\sources\RandomDataGenerator\data";
            _random = new RandomDataGenerator.RandomDataGenerator(path);
        }
        
        public IList<Person> GetPersons(int count, ICollection<IdentificationType> types)
        {
           
            var rmd = new Random();
            var persons = new List<Person>();
            for (int i = 0; i < count; i++)
            {
                persons.Add(
                    new Person {
                        DateOfBirth = DateTime.Now.AddDays(rmd.Next(0, 15000)),
                        FirstName = _random.GetRandomFirtNames(1)[0],
                        LastName = _random.GetRamdomLastNames(1)[0],
                        //IdentificationType = types.ToList()[rmd.Next(0, types.Count)],
                        IdentificationNumber = GetRamdonString(15),
                        IdentificationTypeId = types.ToList()[rmd.Next(0, types.Count)].IdentificationTypeId,
                        Email = string.Format("{0}@{1}.com", _random.GetRandomWords(1)[0], _random.GetRandomWords(1)[0]),
                        Active = rmd.Next(0, 10) > 5? true: false
                    });
            }
            return persons;
        }

        internal IEnumerable<Athlete> GetAthletes(List<Person> persons, List<Category> categories)
        {
            var rmd = new Random();
            return persons.OrderBy(p => p.LastName)
                .Take(rmd.Next(1, persons.Count - 1))
                .Select(p => new Athlete
                {
                    Active = p.Active,
                    DateOfBirth = p.DateOfBirth,
                    Email = p.Email,
                    FirstName = p.FirstName,
                    IdentificationNumber = p.IdentificationNumber,
                    //IdentificationType = p.IdentificationType,
                    IdentificationTypeId = p.IdentificationTypeId,
                    LastName = p.LastName,
                    Photo = p.Photo,
                    Categories = categories.Take(rmd.Next(1, categories.Count -1))
                                .Select(cat => new AthleteCategory {CategoryId = cat.CategoryId, InitialScore = rmd.NextDouble() * 4000 }).ToList()
                });
        }


        public string GetRamdonString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(0, s.Length)]).ToArray());
        }


     
        internal IEnumerable<SurfEvent> GetEvents(int count)
        {
            var words = _random.GetRandomWords(count * 2);
            var events = new List<SurfEvent>();
            var random = new Random();
            for (int i = 0; i < count; i++)
            {
                events.Add(new SurfEvent {
                    Name = words[random.Next(0, words.Length)],
                    Description = words[random.Next(0, words.Length)],
                    Comments = words[random.Next(0, words.Length)] + words[random.Next(0, words.Length)],
                    End = DateTime.Now.AddDays(5),
                    Start = DateTime.Now.AddDays(random.Next(-5, 20)),
                    Status = (Models.Application.EventStatus) random.Next(1, 4),
                    Place = words[random.Next(0, words.Length)]
                });
            }

            return events;
        }
    }
}

namespace WaveTube.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelupdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AthleteCategories",
                c => new
                    {
                        AthleteId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        InitialScore = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.AthleteId, t.CategoryId })
                .ForeignKey("dbo.Athletes", t => t.AthleteId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.AthleteId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Athletes",
                c => new
                    {
                        AthleteId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        IdentificationTypeId = c.Int(nullable: false),
                        IdentificationNumber = c.String(),
                        Email = c.String(),
                        Photo = c.Binary(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AthleteId)
                .ForeignKey("dbo.IdentificationTypes", t => t.IdentificationTypeId)
                .Index(t => t.IdentificationTypeId);
            
            CreateTable(
                "dbo.IdentificationTypes",
                c => new
                    {
                        IdentificationTypeId = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.IdentificationTypeId);
            
            CreateTable(
                "dbo.HeatScores",
                c => new
                    {
                        HeatScoreId = c.Int(nullable: false, identity: true),
                        HeatId = c.Int(nullable: false),
                        JudgeId = c.Int(nullable: false),
                        AthleteId = c.Int(nullable: false),
                        Score = c.Double(nullable: false),
                        Judge_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.HeatScoreId)
                .ForeignKey("dbo.Athletes", t => t.AthleteId)
                .ForeignKey("dbo.Heats", t => t.HeatId)
                .ForeignKey("dbo.Users", t => t.Judge_UserId)
                .Index(t => t.HeatId)
                .Index(t => t.AthleteId)
                .Index(t => t.Judge_UserId);
            
            CreateTable(
                "dbo.Heats",
                c => new
                    {
                        HeatId = c.Int(nullable: false, identity: true),
                        Round = c.String(),
                        Number = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        SurfEventId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        OpenDate = c.DateTime(),
                        CloseDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.HeatId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.SurfEvents", t => t.SurfEventId)
                .Index(t => t.SurfEventId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.HeatAthletes",
                c => new
                    {
                        HeatAthleteId = c.Int(nullable: false, identity: true),
                        HeatId = c.Int(nullable: false),
                        AthleteId = c.Int(nullable: false),
                        ColorCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HeatAthleteId)
                .ForeignKey("dbo.Athletes", t => t.AthleteId)
                .ForeignKey("dbo.Heats", t => t.HeatId)
                .Index(t => t.HeatId)
                .Index(t => t.AthleteId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Comments = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.SurfEvents",
                c => new
                    {
                        SurfEventId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Comments = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Place = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SurfEventId);
            
            CreateTable(
                "dbo.EventCategories",
                c => new
                    {
                        EventCategoryId = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        SurfEventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventCategoryId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.SurfEvents", t => t.SurfEventId)
                .Index(t => t.CategoryId)
                .Index(t => t.SurfEventId);
            
            CreateTable(
                "dbo.EventCategoryAthletes",
                c => new
                    {
                        EventCategoryAthleteId = c.Int(nullable: false, identity: true),
                        EventCategoryId = c.Int(nullable: false),
                        AthleteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventCategoryAthleteId)
                .ForeignKey("dbo.Athletes", t => t.AthleteId)
                .ForeignKey("dbo.EventCategories", t => t.EventCategoryId, cascadeDelete: true)
                .Index(t => t.EventCategoryId)
                .Index(t => t.AthleteId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 20),
                        Password = c.String(),
                        Rol = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        IdentificationTypeId = c.Int(nullable: false),
                        IdentificationNumber = c.String(),
                        Email = c.String(),
                        Photo = c.Binary(),
                        Active = c.Boolean(nullable: false),
                        SurfEvent_SurfEventId = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.IdentificationTypes", t => t.IdentificationTypeId)
                .ForeignKey("dbo.SurfEvents", t => t.SurfEvent_SurfEventId)
                .Index(t => t.UserName, unique: true)
                .Index(t => t.IdentificationTypeId)
                .Index(t => t.SurfEvent_SurfEventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HeatScores", "Judge_UserId", "dbo.Users");
            DropForeignKey("dbo.HeatScores", "HeatId", "dbo.Heats");
            DropForeignKey("dbo.Users", "SurfEvent_SurfEventId", "dbo.SurfEvents");
            DropForeignKey("dbo.Users", "IdentificationTypeId", "dbo.IdentificationTypes");
            DropForeignKey("dbo.Heats", "SurfEventId", "dbo.SurfEvents");
            DropForeignKey("dbo.EventCategories", "SurfEventId", "dbo.SurfEvents");
            DropForeignKey("dbo.EventCategories", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.EventCategoryAthletes", "EventCategoryId", "dbo.EventCategories");
            DropForeignKey("dbo.EventCategoryAthletes", "AthleteId", "dbo.Athletes");
            DropForeignKey("dbo.Heats", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.AthleteCategories", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.HeatAthletes", "HeatId", "dbo.Heats");
            DropForeignKey("dbo.HeatAthletes", "AthleteId", "dbo.Athletes");
            DropForeignKey("dbo.HeatScores", "AthleteId", "dbo.Athletes");
            DropForeignKey("dbo.Athletes", "IdentificationTypeId", "dbo.IdentificationTypes");
            DropForeignKey("dbo.AthleteCategories", "AthleteId", "dbo.Athletes");
            DropIndex("dbo.Users", new[] { "SurfEvent_SurfEventId" });
            DropIndex("dbo.Users", new[] { "IdentificationTypeId" });
            DropIndex("dbo.Users", new[] { "UserName" });
            DropIndex("dbo.EventCategoryAthletes", new[] { "AthleteId" });
            DropIndex("dbo.EventCategoryAthletes", new[] { "EventCategoryId" });
            DropIndex("dbo.EventCategories", new[] { "SurfEventId" });
            DropIndex("dbo.EventCategories", new[] { "CategoryId" });
            DropIndex("dbo.HeatAthletes", new[] { "AthleteId" });
            DropIndex("dbo.HeatAthletes", new[] { "HeatId" });
            DropIndex("dbo.Heats", new[] { "CategoryId" });
            DropIndex("dbo.Heats", new[] { "SurfEventId" });
            DropIndex("dbo.HeatScores", new[] { "Judge_UserId" });
            DropIndex("dbo.HeatScores", new[] { "AthleteId" });
            DropIndex("dbo.HeatScores", new[] { "HeatId" });
            DropIndex("dbo.Athletes", new[] { "IdentificationTypeId" });
            DropIndex("dbo.AthleteCategories", new[] { "CategoryId" });
            DropIndex("dbo.AthleteCategories", new[] { "AthleteId" });
            DropTable("dbo.Users");
            DropTable("dbo.EventCategoryAthletes");
            DropTable("dbo.EventCategories");
            DropTable("dbo.SurfEvents");
            DropTable("dbo.Categories");
            DropTable("dbo.HeatAthletes");
            DropTable("dbo.Heats");
            DropTable("dbo.HeatScores");
            DropTable("dbo.IdentificationTypes");
            DropTable("dbo.Athletes");
            DropTable("dbo.AthleteCategories");
        }
    }
}

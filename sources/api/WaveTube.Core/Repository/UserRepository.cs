﻿using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Data.Contracts;
using WaveTube.Models.Core;
using System.Data.Entity;


namespace WaveTube.Core.Managers
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IDbContext context)
            : base(context)
        {



        }

        public override IQueryable<User> GetAll()
        {
            return Db.Set<User>()
                .Include(usr => usr.IdentificationType);                
        }
    }
}

﻿using WaveTube.Core.Contracts.Repository;
using WaveTube.Data.Contracts;
using WaveTube.Models.Core;

namespace WaveTube.Core.Managers
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(IDbContext context) 
            : base(context)
        {
        }
    }
}

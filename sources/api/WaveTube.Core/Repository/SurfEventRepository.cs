﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Data.Contracts;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Managers
{
    public class SurfEventRepository : BaseRepository<SurfEvent> , ISurfEventRepository
    {
        public SurfEventRepository(IDbContext context)
            :base(context)
        {
          
        }

        public override IQueryable<SurfEvent> GetAll()
        {
            return Db.Set<SurfEvent>()
                .Include(e => e.Categories);
                
       
        }

        public override SurfEvent Get(params object[] arguments)
        {
            
            var id = (int)arguments[0];
            return Db.Set<SurfEvent>()
                .Include(e => e.Categories.Select(x => x.Category))
                .Include(e => e.Categories.Select(x => x.Athletes.Select(ath=>ath.Athlete)))             
                .Include(e => e.Heats.Select(h => h.Athletes.Select(a=> a.Athlete)))
                .FirstOrDefault(e => e.SurfEventId == id);
        }

        public override void Modify(SurfEvent surfEvent)
        {
            var currentEvent = this.Get(surfEvent.SurfEventId);
            if (currentEvent == null)
            {
                return;
            }

            ClearEventCircularReferences(surfEvent);

            HandleDeletedItems(surfEvent, currentEvent);

            foreach (var category in surfEvent.Categories)
            {
                currentEvent.Categories.Add(category);
            }

            foreach (var heat in surfEvent.Heats)
            {
                currentEvent.Heats.Add(heat);
            }

            Db.Entry(currentEvent).CurrentValues.SetValues(surfEvent);
        }

        private void HandleDeletedItems(SurfEvent surfEvent, SurfEvent currentEvent)
        {
           
            for (int i = currentEvent.Categories.Count - 1; i >= 0; i--)
            {
                var currentCategory = currentEvent.Categories.ElementAt(i);
                for (int j = currentCategory.Athletes.Count -1 ; j >= 0; j--)
                {
                    var currentAthelte = currentCategory.Athletes.ElementAt(j);
                    Db.Set<EventCategoryAthlete>().Remove(currentAthelte);
                }
                Db.Set<EventCategory>().Remove(currentCategory);
            }

            for (int i = currentEvent.Heats.Count - 1; i >= 0; i--)
            {
                var currentHeat = currentEvent.Heats.ElementAt(i);
                for (int j = currentHeat.Athletes.Count - 1; j >= 0; j--)
                {
                    var currentAthelte = currentHeat.Athletes.ElementAt(j);
                    Db.Set<HeatAthlete>().Remove(currentAthelte);
                }
                Db.Set<Heat>().Remove(currentHeat);
            }
        }


 
        private void ClearEventCircularReferences(SurfEvent surfEvent)
        {

            foreach (var category in surfEvent.Categories)
            {
                category.Event = null;
                category.Category = null;
                foreach (var catAthlete in category.Athletes)
                {
                    catAthlete.Athlete = null;
                    catAthlete.EventCategory = null;
                }
            }

         

            foreach (var heat in surfEvent.Heats)
            {
                heat.Category = null;
                heat.Event = null;
                foreach (var heatAthlete in heat.Athletes)
                {
                    heatAthlete.Athlete = null;
                    heatAthlete.Heat = null;                   
                }


                if (heat.Scores == null)
                {
                    return;
                }

                foreach (var score in heat.Scores)
                {
                    score.Heat = null;
                    score.Athelte = null;
                    score.Judge = null;
                }
            }                
        }       
      
    }
}

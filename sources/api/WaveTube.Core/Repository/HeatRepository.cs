﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Data.Contracts;
using WaveTube.Models.Core;

namespace WaveTube.Core.Managers
{
    public class HeatRepository : BaseRepository<Heat>, IHeatRepository
    {

        private const int HEAT_DURATION = 10;
        private const int MAX_ATHLETES_PER_HEATS = 4;
        private const int MIN_ATHLETES_PER_HEATS = 3;
        public HeatRepository(IDbContext context)
            : base(context)
        {

        }

        public override IQueryable<Heat> GetAll()
        {
            return Db.Set<Heat>()
                .Include(a => a.Athletes);
        }

        public override Heat Get(params object[] arguments)
        {
            return base.Get(arguments);
        }

        public override void Modify(Heat heat)
        {
            base.Modify(heat);
        }


        public Heat GetCurrentHeat(int eventId)
        {
            return Db.Set<Heat>()
                   .Include(heat => heat.Category)
                   .Include(heat => heat.Scores.Select(s => s.Athelte))
                   .Include(heat => heat.Scores.Select(s => s.Judge))
                   .Include(heat => heat.Athletes.Select(a => a.Athlete))
                   .Include(heat => heat.Penalties.Select(p => p.Athelte))
                   .Include(heat => heat.Penalties.Select(p => p .Judge))
                   .FirstOrDefault(heat => heat.SurfEventId == eventId && heat.Status == HeatStatus.Pending);
        }

        public Heat GetNextHeat(int eventId, string round, int currentHeatId)
        {
            return Db.Set<Heat>()
                   .Include(heat => heat.Scores)
                   .Include(heat => heat.Category)
                   .Include(heat => heat.Scores.Select(s => s.Athelte))
                   .Include(heat => heat.Scores.Select(s => s.Judge))
                   .Include(heat => heat.Athletes.Select(a => a.Athlete))
                   .Include(heat => heat.Penalties.Select(p => p.Athelte))
                   .Include(heat => heat.Penalties.Select(p => p.Judge))
                   .OrderBy(heat => heat.Round).ThenBy(heat=> heat.Number)
                   .FirstOrDefault(heat => heat.SurfEventId == eventId && heat.Status == HeatStatus.Pending && heat.HeatId > currentHeatId);
        }


        public Heat GetPreviousHeat(int eventId)
        {
            return Db.Set<Heat>()
                   .Include(heat => heat.Scores)
                   .Include(heat => heat.Category)
                   .Include(heat => heat.Scores.Select(s => s.Athelte))
                   .Include(heat => heat.Scores.Select(s => s.Judge))
                   .Include(heat => heat.Athletes.Select(a => a.Athlete))
                   .Include(heat => heat.Penalties.Select(p => p.Athelte))
                   .Include(heat => heat.Penalties.Select(p => p.Judge))
                   .OrderByDescending(heat => heat.CloseDate)
                   .FirstOrDefault(heat => heat.Status == HeatStatus.Closed);
        }

        public void SaveHeatScore(HeatScore heatScore)
        {
            if (heatScore.Score == 0 )
            {
                return;
            }
            
            Db.Set<HeatScore>().Add(heatScore);
            Db.SaveChanges();
        }
        public void SaveHeatPenalty(HeatAthletePenalty heatAthletePenalty)
        {
            Db.Set<HeatAthletePenalty>().Add(heatAthletePenalty);
            Db.SaveChanges();
        }
    }
}

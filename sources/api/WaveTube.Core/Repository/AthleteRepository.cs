﻿using System.Data.Entity;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Data.Contracts;
using WaveTube.Models.Core;

namespace WaveTube.Core.Managers
{
    public class AthleteRepository : BaseRepository<Athlete> , IAthleteRepository
    {
        public AthleteRepository(IDbContext context)
            :base(context)
        {
          
        }

        public override IQueryable<Athlete> GetAll()
        {
            return Db.Set<Athlete>()
                .Include(a => a.Categories)
                //.Include(a => a.Events)
                .Include(a => a.IdentificationType);
               
        }

        public override Athlete Get(params object[] arguments)
        {
            var id = (int) arguments[0];
            return Db.Set<Athlete>()
                 .Include(a => a.Categories)
                 .Include(a => a.Categories.Select(c=> c.Category))
                .Include(a => a.IdentificationType)
                .FirstOrDefault(a => a.AthleteId == id);
        }

        public override void Modify(Athlete athlete)
        {
            var curentAthlete = Db.Set<Athlete>().Find(athlete.AthleteId);
            if (curentAthlete == null)
            {
                return ;
            }

            Db.Entry(curentAthlete).CurrentValues.SetValues(athlete);

            for (int i = curentAthlete.Categories.Count -1 ; i >= 0; i--)
            {
                var currentCategory = curentAthlete.Categories.ElementAt(i);
                if (!athlete.Categories.Any(at => at.CategoryId == currentCategory.CategoryId))
                {
                    Db.Entry(currentCategory).State = EntityState.Deleted;
                }
            }

            foreach (var category in athlete.Categories)
            {
                var context = Db.Set<AthleteCategory>();
                var currentCategory = context.SingleOrDefault(c => c.CategoryId == category.CategoryId && c.AthleteId == category.AthleteId);
                if (currentCategory == null)
                {
                    Db.Set<AthleteCategory>().Add(new AthleteCategory
                    {
                        AthleteId = athlete.AthleteId,
                        CategoryId = category.CategoryId,
                        InitialScore = category.InitialScore
                    });
                }
                else
                {
                    Db.Entry(currentCategory).CurrentValues.SetValues(category);
                }
            }
        }
    }
}

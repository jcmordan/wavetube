﻿using System.Collections.Generic;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Application;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Rules
{
    public class CategoryRules : BaseRules<Category>, ICategoryRules
    {
        private readonly ICategoryRepository _repository;
        public CategoryRules(ICategoryRepository repository)           
        {
            _repository = repository;
        }

        public Category Add(Category model, AuthenticationContext authContext)
        {
            Validate(model);
            Authorize(ApplicationResource, Permission.Create, authContext);
            _repository.Add(model);
            return model;
        }

        public Category Get(int id, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.Get(id);
        }

        public ICollection<Category> GetAll(AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.GetAll().ToList();
        }

        public Category Modify(Category model, AuthenticationContext authContext)
        {
            Validate(model);
            Authorize(ApplicationResource, Permission.Update, authContext);
            _repository.Modify(model);
            return model;
        }

        public ValidationException Validate(Category model)
        {
            ValidationException exeption = new ValidationException("Invalid value");
            if (model == null)
            {
                exeption.AddError("model", ValidationError.Missing);
            }

            if (exeption.Errors.Count > 0)
            {
                throw exeption;
            }

            return exeption;
        }

        public bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext)
        {
            return true;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}

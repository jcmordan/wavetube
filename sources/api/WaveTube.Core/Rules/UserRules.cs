﻿using System;
using System.Collections.Generic;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Application;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Rules
{
    public class UserRules : PersonRules<User>, IUserRules
    {
        private const int MIN_USERNAME_LENGTH = 3;
        private const int MAX_USERNAME_LENGTH = 20;

        private readonly IUserRepository _repository;
        public UserRules(IUserRepository repository)
        {
            _repository = repository;
        }

        public override User Get(int id, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.Get(new int[] { 1 });
        }

        public override ICollection<User> GetAll(AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.GetAll().ToList();
        }

        public override User Add(User model, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Create, authContext);
            Validate(model);
            _repository.Add(model);
            _repository.SaveChanges();
            return model;
        }

        public override User Modify(User model, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Update, authContext);
            Validate(model);
            _repository.Add(model);
            _repository.SaveChanges();
            return model;
        }

        public override bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext)
        {
            return true;
        }

        public override ValidationException Validate(User user)
        {
            var exception = base.Validate(user);

            if (string.IsNullOrWhiteSpace(user.UserName))
            {
                exception.AddError("UserName", ValidationError.Empty);
            }

            if (user.UserName.Length < MIN_USERNAME_LENGTH)
            {
                exception.AddError("UserName", ValidationError.ShorterThanExpected);
            }

            if (user.UserName.Length < MAX_USERNAME_LENGTH)
            {
                exception.AddError("UserName", ValidationError.LargerThanExpected);
            }

            return exception;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Application;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Rules
{
    public class PersonRules<T> : BaseRules<T>, IPersonRules<T>
        where T : Person

    {
        public virtual T Add(T model, AuthenticationContext authContext)
        {
            throw new NotImplementedException();
        }

        public virtual bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext)
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual T Get(int id, AuthenticationContext authContext)
        {
            throw new NotImplementedException();
        }

        public virtual ICollection<T> GetAll(AuthenticationContext authContext)
        {
            throw new NotImplementedException();
        }

        public virtual T Modify(T model, AuthenticationContext authContext)
        {
            throw new NotImplementedException();
        }

        public virtual ValidationException Validate(T model)
        {
            var exception = new ValidationException("errors");
            if (model.IdentificationTypeId <= 0)
            {
                exception.AddError("IdentificationType", ValidationError.NotAllowed);
            }

            if (model.DateOfBirth <= DateTime.Now)
            {
                exception.AddError("DayOfBirth", ValidationError.RangeInvalid);
            }

            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                exception.AddError("FirstName", ValidationError.Empty);
            }

            if (string.IsNullOrWhiteSpace(model.LastName))
            {
                exception.AddError("FirstName", ValidationError.Empty);
            }

            return exception;
        }
    }
}

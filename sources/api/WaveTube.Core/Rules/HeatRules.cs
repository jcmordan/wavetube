﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Application;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Rules
{
    public class HeatRules : BaseRules<Heat>, IHeatRules
    {
        private readonly IHeatRepository _repository;
        private double MAX_ATHLETES_PER_HEATS = 4;
        private int HEAT_DURATION = 10;

        public HeatRules(IHeatRepository repository)            
        {
           
            _repository = repository;
        }

        public Heat Get(int id, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.Get(id);        
        }

        public ICollection<Heat> GetAll(AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.GetAll().ToList();
        }

        public Heat Add(Heat heat, AuthenticationContext authContext)
        {
            Validate(heat);

            Authorize(ApplicationResource, Permission.Create, authContext);

            _repository.Add(heat);
            return heat;
        }

        public Heat Modify(Heat heat, AuthenticationContext authContext)
        {
            Validate(heat);

            Authorize(ApplicationResource, Permission.Update, authContext);

            _repository.Add(heat);
            return heat;
        }
   
        public virtual ValidationException Validate(Heat heat)
        {
            ValidationException exeption = new ValidationException("Invalid value");
            if (heat == null)
            {
                exeption.AddError("model", ValidationError.Missing);
            }

            if (exeption.Errors.Count > 0) {
                throw exeption;
            }

            return exeption; 
        }

        public bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext)
        {
            return true;
        }

      

        public ICollection<Heat> BuildHeats(ICollection<EventCategory> eventCategories)
        {
            var heatList = new List<Heat>();
            foreach (var category in eventCategories)
            {
                heatList.AddRange(BuildCategoryHeats(category));
            }
            return heatList;
        }

        private IEnumerable<Heat> BuildCategoryHeats(EventCategory category)
        {
            var categoryHeats = new List<Heat>();

            var totalHeats = (1.0 * category.Athletes.Count / MAX_ATHLETES_PER_HEATS);
            for (int i = 0; i < totalHeats; i++)
            {
                categoryHeats.Add(new Heat
                {
                    Round = "1",
                    Number = i + 1,
                    Duration = HEAT_DURATION,
                    CategoryId = category.CategoryId,
                    Status = HeatStatus.Pending,
                    SurfEventId = category.SurfEventId,
                    Athletes = new List<HeatAthlete>()
                });
            }

            var athletes = GetAthleteByCategory(category);

            if (athletes == null || athletes.Count == 0)
            {
                return categoryHeats;
            }

            SetAthletesToHeat(categoryHeats, athletes);

            return categoryHeats;
        }

        private List<Athlete> GetAthleteByCategory(EventCategory category)
        {
            var athleteIdList = category.Athletes.Select(cat => cat.AthleteId).ToList();
            var athletes = _repository.Db.Set<Athlete>()
                   .Where(at => athleteIdList.Contains(at.AthleteId))
                   .Include(at => at.Categories)
                   .Include(at => at.Scores)
                   .Include(at => at.Scores.Select(s => s.Heat))
                   .ToList();

            athletes.ForEach(at =>
            {
                var initialScore = at.Categories.Where(cat => cat.CategoryId == category.CategoryId).Sum(cat => cat.InitialScore);
                var eventsScore = at.Scores.Where(s => s.Heat.CategoryId == category.CategoryId).Sum(sc => sc.Score);
                at.CurrentScore = initialScore + eventsScore;
            });

            return athletes.OrderByDescending(at => at.CurrentScore).ToList();
        }

        private void SetAthletesToHeat(List<Heat> heats, List<Athlete> athletes)
        {
            if (athletes.Count == 0)
            {
                return;
            }

            var allHeatIsFull = heats.Any(h => h.Athletes.Count == MAX_ATHLETES_PER_HEATS);

            if (allHeatIsFull)
            {
                return;
            }

            foreach (var heat in heats)
            {
                var athlete = athletes[0];
                heat.Athletes.Add(new HeatAthlete
                {
                    Athlete = athlete,
                    AthleteId = athlete.AthleteId,
                    ColorCode = (AthelteColor)heat.Athletes.Count + 1
                });

                athletes.Remove(athlete);

                if (athletes.Count == 0)
                {
                    break;
                }
            }

            SetAthletesToHeat(heats, athletes);
        }

        public Heat GetCurrentHeat(int eventId) => _repository.GetCurrentHeat(eventId);

        public Heat GetPreviousHeat(int eventId) => _repository.GetPreviousHeat(eventId);

        public Heat GetNextHeat(int eventId, string round, int currentHeatId) => 
            _repository.GetNextHeat(eventId, round, currentHeatId);

        public void SaveHeatScore(HeatScore heatScore)
            => _repository.SaveHeatScore(heatScore);

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Data.Contracts;
using WaveTube.Models.Application;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Rules
{
    public class SurfEventRules: BaseRules<SurfEvent>, ISurfEventRules
    {
        public readonly ISurfEventRepository _repository;
        public SurfEventRules(ISurfEventRepository repository)           
        {
            _repository = repository;
        }

        public SurfEvent Add(SurfEvent model, AuthenticationContext authContext)
        {
            Validate(model);
            Authorize(ApplicationResource, Permission.Create, authContext);
            _repository.Add(model);
            _repository.SaveChanges();

            return model;
        }

        public SurfEvent Get(int id, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.Get(id);
        }

        public ICollection<SurfEvent> GetAll(AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.GetAll().ToList();
        }

        public IEnumerable<SurfEvent> GetAll(int? status, int? count)
        {
            var query = _repository.GetAll();
            if (status.HasValue)
            {
                query = query.Where(evt => evt.Status == (EventStatus) status);
            }

            if (count.HasValue)
            {
                query = query.Take(count.Value);
            }
            return query.ToList();
        }    
      

        public SurfEvent OpenHeat(int id)
        {
            SurfEvent surfEvent = _repository.Get(id);
            surfEvent.Status = EventStatus.Opened;
            _repository.Modify(surfEvent);
            _repository.SaveChanges();

            return surfEvent;
        }

        public SurfEvent Modify(SurfEvent model, AuthenticationContext authContext)
        {
            Validate(model);
            Authorize(ApplicationResource, Permission.Create, authContext);
            _repository.Modify(model);
            _repository.SaveChanges();
            return model;
        }

        public  ValidationException Validate(SurfEvent surftEvent)
        {
            var exeption = new ValidationException("SurfEvent");
            if (surftEvent == null)
            {
                exeption.AddError("model", ValidationError.Missing);
            }

            exeption.AddErrors("InvalidStatus", ValidateStatus(surftEvent));


            if (exeption.Errors.Count > 0)
            {
                throw exeption;
            }

            return exeption;
        }

        public bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext)
        {
            return true;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        private ISet<ValidationError> ValidateStatus(SurfEvent surftEvent)
        {
            var error = new HashSet<ValidationError>();
            // todo change this property to enum
            switch (surftEvent.Status)
            {
                case EventStatus.Pending:
                    break;
                case EventStatus.Opened:
                    if (surftEvent.Heats.Count == 0)
                    {
                        error.Add(ValidationError.UnexpectedValue);
                    }
                    break;
                default:
                    error.Add(ValidationError.NotAllowed);
                    break;
                   
            }

            return error;
        }

      
    }
}

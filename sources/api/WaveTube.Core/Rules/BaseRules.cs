﻿using WaveTube.Models.Application;
using WaveTube.Models.Core;

namespace WaveTube.Core.Rules
{
    public abstract class BaseRules<T>
        where T : class
    {
        protected virtual ApplicationResource ApplicationResource
        {
            get
            {
                var type = typeof(T);
                if (type == typeof(Person))
                {
                    return ApplicationResource.Users;
                }

                return ApplicationResource.All;
            }
        }
       
    }
}

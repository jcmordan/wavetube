﻿using System;
using System.Collections.Generic;
using System.Linq;
using WaveTube.Core.Contracts.Repository;
using WaveTube.Core.Contracts.Rules;
using WaveTube.Models.Application;
using WaveTube.Models.Core;
using WaveTube.Models.Exceptions;

namespace WaveTube.Core.Rules
{
    public class AthleteRules: BaseRules<Athlete>, IAthleteRules
    {
        private readonly IAthleteRepository _repository;
        public AthleteRules(IAthleteRepository repository)                    
        {
            _repository = repository;
        }

        public Athlete Add(Athlete model, AuthenticationContext authContext)
        {
            Validate(model);
            Authorize(ApplicationResource, Permission.Create, authContext);
            _repository.Add(model);
            return model;
        }
      

        public Athlete Get(int id, AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.Get(id);
        }

        public ICollection<Athlete> GetAll(AuthenticationContext authContext)
        {
            Authorize(ApplicationResource, Permission.Read, authContext);
            return _repository.GetAll().ToList();
        }

        public Athlete Modify(Athlete model, AuthenticationContext authContext)
        {
            Validate(model);
            Authorize(ApplicationResource, Permission.Update, authContext);
            _repository.Modify(model);
            return model;
        }

        public ValidationException Validate(Athlete model)
        {
            ValidationException exeption = new ValidationException("Invalid value");
            if (model == null)
            {
                exeption.AddError("model", ValidationError.Missing);
            }

            if (exeption.Errors.Count > 0)
            {
                throw exeption;
            }

            return exeption;
        }

        public bool Authorize(ApplicationResource resoruce, Permission permission, AuthenticationContext authContext)
        {
            return true;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}

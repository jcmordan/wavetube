﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTube.I18n.Contracts
{
    public interface Ii18nController
    {
        string Get(string key);
        string Get(string key, KeyValuePair<string, string> replacement);
    }
}

﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace RandomDataGenerator
{
    public class RandomDataGenerator
    {
       private string[] _words { get; }
        private Random _random;

        public RandomDataGenerator(string path)
        {
            this._words = ReadFile(path);
            _random = new Random();
        }

        private string[] ReadFile(string path)
        {
            var file = $"{path}\\words.json";
            if (!System.IO.File.Exists(file))
                return new string[] { };

            return System.IO.File.ReadAllLines(file);
        }

        public string[] GetRandomFirtNames(int count)
        {
            return _words.OrderBy(item => _random.Next())
                .Take(count).ToArray();
        }

        public string[] GetRamdomLastNames(int count)
        {
            return _words.OrderBy(item => _random.Next())
                .Take(count).ToArray();
        }

        public string[] GetRandomWords(int count)
        {
            return _words.OrderBy(item => _random.Next())
                .Take(count).ToArray();
        }
    }
}
